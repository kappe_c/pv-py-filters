import numpy as np

import vtkmodules
from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import vtkMultiBlockDataSet, vtkPointSet

from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain


@smproxy.filter(label="Block Per Bin")
@smproperty.input(name="Points")
@smdomain.datatype(dataTypes=["vtkPointSet"], composite_data_supported=False)
class BlockPerBin(VTKPythonAlgorithmBase):
  """ Create a multiblock dataset with one block for each subset of the input corresponding to a certain scalar range
  
  Define regular bins (at least two) by setting a scalar attribute to consider,
  a first boundary value, the width of the bins and the number.
  The points of the dataset are then divided into blocks depending on the bin in which the scalar
  at any point falls.
  """
  
  def __init__ (self):
    VTKPythonAlgorithmBase.__init__(
      self, nInputPorts=1, nOutputPorts=1, outputType="vtkMultiBlockDataSet")
    self._attrName = ""
    self._numBins = 2
    self._firstSeparator = 0.5 # part of the right bin
    self._binWidth = 1.
  
  #------------------------------------------------------------------------------------------------#
  
  @smproperty.xml("""
<StringVectorProperty name="ScalarAttribute" number_of_elements="1" command="SetScalarAttribute">
  <ArrayListDomain name="array_list" default_values="0">
    <RequiredProperties><Property function="Input" name="Points"/></RequiredProperties>
  </ArrayListDomain>
  <Documentation>Select the scalar attribute to consider.</Documentation>
</StringVectorProperty>""")
  def SetScalarAttribute(self, val):
    self._attrName = val
    self.Modified()
  
  
  @smproperty.intvector(name="NumBins", default_values=2)
  @smdomain.intrange(min=1) # 1 selects automagically
  def SetNumBins(self, val):
    self._numBins = val
    self.Modified()
  
  
  @smproperty.doublevector(name="FirstSeparator", default_values=0.5)
  def SetFirstSeparator(self, val):
    self._firstSeparator = val
    self.Modified()
  
  
  @smproperty.doublevector(name="BinWidth", default_values=1.)
  @smdomain.doublerange(min=0.)
  def SetBinWidth(self, val):
    self._binWidth = val
    self.Modified()
  
  #------------------------------------------------------------------------------------------------#
  
  def execute(self, iPointSet, oBlocks):
    scalars = iPointSet.PointData[self._attrName]
    if type(scalars) == dsa.VTKNoneArray:
      print('%s: there is no scalar point data named "%s"' % (type(self).__name__, self._attrName))
      #scaAtts = list(filter(lambda k, v: v.ndim == 1, iPointSet.PointData.items()))
      scaAtts = []
      for name in iPointSet.PointData.keys():
        arr = iPointSet.PointData[name]
        if arr.ndim == 1:
          scaAtts.append((name, arr))
      if len(scaAtts) == 0:
        print('to avoid a crash, we simply wrap the input into a multiblock dataset with one block')
        oBlocks.SetBlock(0, iPointSet.VTKObject)
        return 1
      self._attrName, scalars = scaAtts[0]
      if len(scaAtts) == 1:
        print('assuming "%s" is meant (the only scalar attribute)' % self._attrName)
      else:
        print('assuming "%s" is meant (the first scalar attribute)' % self._attrName)
    
    numPts = len(scalars)
    point2block = np.empty((numPts), dtype=int)
    
    numBins = self._numBins
    if numBins == 1:
      numBins = round(scalars.ptp()) + 1
      print("Automatically set *Num Bins* to %d based on *%s* range" % (numBins, self._attrName))
    
    off = 0.5 - self._firstSeparator
    binWidthInv = 1. / self._binWidth
    bMax = numBins - 1
    for p, scalar in enumerate(scalars):
      b = round((scalar + off) * binWidthInv)
      # the result of an integer division is still a float if the first argument is a float
      point2block[p] = 0 if b < 0 else (bMax if b > bMax else b)
    
    MyClass = type(iPointSet.VTKObject)
    iPoints = iPointSet.Points
    
    #numNanPoints = np.count_nonzero(np.isnan(iPoints))
    #if numNanPoints > 0:
      #print('Warning: BlockPerBin (pre-split): %d nan point components' % numNanPoints)
      ##for pt in iPoints:
        ##print('%7f %7f %7f' % (pt[0], pt[1], pt[2]))
    #numNanScalars = np.count_nonzero(np.isnan(scalars))
    #if numNanScalars > 0:
      #print('Warning: BlockPerBin (pre-split): %d nan values in scalars' % numNanScalars)
      ##for val in scalars:
        ##print(val)
    
    iPointData = iPointSet.PointData
    # Sometimes there is junk data from upstream that we filter out here.
    # & is element-wise logical-and
    point2isfinite = (
      np.isfinite(iPoints[:, 0]) & np.isfinite(iPoints[:, 1]) & np.isfinite(iPoints[:, 2]) &
      np.isfinite(scalars))
    for b in range(numBins):
      blockRaw = MyClass()
      block = dsa.WrapDataObject(blockRaw)
      # NOTE field data is currently not carried over
      #mask = point2block == b
      mask = (point2block == b) & point2isfinite
      block.Points = iPoints[mask]
      blockData = block.PointData
      for name in iPointData.keys(): # there is no .items() :-(
        blockData.append(iPointData[name][mask], name)
      oBlocks.SetBlock(b, blockRaw)
    
    #print("<-- END", type(self).__name__)
    return 1
  
  #------------------------------------------------------------------------------------------------#
  
  def RequestData(self, request, iInfoVecTuple, oInfoVec):
    #print("--> BEGIN", type(self).__name__)
    iPointSet = dsa.WrapDataObject(vtkPointSet.GetData(iInfoVecTuple[0]))
    oBlocks = dsa.WrapDataObject(vtkMultiBlockDataSet.GetData(oInfoVec))
    return self.execute(iPointSet, oBlocks)
