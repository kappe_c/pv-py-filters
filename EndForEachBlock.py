from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import vtkMultiBlockDataSet, vtkDataObject
#from vtkmodules.vtkCommonExecutionModel import vtkStreamingDemandDrivenPipeline


from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain

import BegForEachBlock



@smproxy.filter(label="End For Each Block")
@smproperty.input(name="Begin", port_index=1)
@smdomain.datatype(dataTypes=["vtkDataSet"])
@smproperty.input(name="LastFilter", port_index=0)
@smdomain.datatype(dataTypes=["vtkDataSet"])
class EndForEachBlock(VTKPythonAlgorithmBase):
  """End a for-each block started with a "BegForEachBlock" filter."""
  
  def __init__ (self):
    VTKPythonAlgorithmBase.__init__(self, nInputPorts=2, nOutputPorts=1,
                                    outputType="vtkMultiBlockDataSet")
    self._foo = None
  
  
  
  def get_begin_algo(self):
    algo = self.GetInputAlgorithm(1, 0)
    #print(dir(algo))
    while algo is not None and type(algo).__name__ != "BegForEachBlock":
      #print("---------", algo.GetClassName())
      #print("---------", type(algo).__name__)
      algo = algo.GetInputAlgorithm()
    return BegForEachBlock.BegForEachBlock().SafeDownCast(algo)
  
  
  
  def RequestData(self, request, iInfoVecTuple, oInfoVec):
    #print("--> RequestData %s" % type(self).__name__)
    
    beginAlgo = self.get_begin_algo()
    if beginAlgo is None:
      print("Error: the second input must be connected to a BegForEachBlock filter")
      return 0
    
    beginInfo = iInfoVecTuple[1].GetInformationObject(0)
    beginDataset = beginInfo.Get(vtkDataObject.DATA_OBJECT())
    fldDat = dsa.WrapDataObject(beginDataset).FieldData
    numBlocks = fldDat["original number of blocks"][0]
    #print("number of blocks:", numBlocks)
    multiblock = vtkMultiBlockDataSet.GetData(oInfoVec)
    multiblock.SetNumberOfBlocks(numBlocks)
    
    # first block but last filter before this end
    #print("Querying block 1 of %d" % numBlocks)
    assert beginAlgo._nextToPush == 0
    #firstInfo = iInfoVecTuple[0].GetInformationObject(0)
    #firstDataset = firstInfo.Get(vtkDataObject.DATA_OBJECT())
    #multiblock.SetBlock(0, firstDataset)
    
    # Theoretically the first block is already processed (by BegForEachBlock) before this
    # filter is added to trigger processing of the rest but in practice (for now) it is easiest if
    # we always start fresh so we do not forget to update the first block in some cases.
    #for b in range(1, numBlocks):
    for b in range(numBlocks):
      #print("Querying block %d of %d" % (b+1, numBlocks))
      
      innerAlgo = self.GetInputAlgorithm(0, 0)
      while innerAlgo is not None and innerAlgo != beginAlgo:
        innerAlgo.Modified()
        innerAlgo = innerAlgo.GetInputAlgorithm()
      assert innerAlgo == beginAlgo
      beginAlgo._nextToPush = b # these two classes are friends
      beginAlgo.Modified()
      
      #request.Set(vtkStreamingDemandDrivenPipeline.CONTINUE_EXECUTING(), 1)
      self.GetInputAlgorithm(0, 0).Update()
      curDataset = self.GetInputAlgorithm(0, 0).GetOutputDataObject(0) # "pseudo copy"
      BlockType = type(curDataset)
      shallowCopy = BlockType()
      shallowCopy.ShallowCopy(curDataset) # does not return anything
      multiblock.SetBlock(b, shallowCopy)
    
    #request.Remove(vtkStreamingDemandDrivenPipeline.CONTINUE_EXECUTING())
    beginAlgo._nextToPush = 0
    return 1
