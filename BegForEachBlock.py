from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import vtkMultiBlockDataSet, vtkDataObject

from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain


# only concrete ones (currently not complete, only what is most relevant right now)
VTK_DATASET_TYPE_NAMES= [
  "vtkImageData", "vtkRectilinearGrid", "vtkStructuredGrid",
  "vtkUnstructuredGrid", "vtkPolyData",
  "vtkMultiBlockDataSet"
]


#from vtkmodules.util.keys import IntegerKey, MakeKey
#numBlocksKey = MakeKey(IntegerKey, "numBlocks", "BegForEachBlock")

#def NUM_BLOCKS():
  #return numBlocksKey



@smproxy.filter(label="Beg For Each Block")
@smproperty.input(name="Multiblock")
class BegForEachBlock(VTKPythonAlgorithmBase):
  """ Beg for each block of a multiblock dataset ;-) """
  
  def __init__ (self):
    VTKPythonAlgorithmBase.__init__(self, nInputPorts=1, nOutputPorts=1)
    self._blockTypeName = VTK_DATASET_TYPE_NAMES[3]
    self._numBlocks = 0
    self._nextToPush = 0
  
  
  def FillInputPortInformation(self, port, info):
    assert port == 0
    info.Set(self.INPUT_REQUIRED_DATA_TYPE(), "vtkMultiBlockDataSet")
    return 1
  
  
  def FillOutputPortInformation(self, port, info):
    assert port == 0
    info.Set(vtkDataObject.DATA_TYPE_NAME(), self._blockTypeName)
    #info.Set(numBlocksKey, 42)
    return 1
  
  #------------------------------------------------------------------------------------------------#
  
  #@smproperty.stringvector(name="BlockType", default_values="vtkUnstructuredGrid")
  #@smdomain.datatype(["vtkUnstructuredGrid", "vtkRectilinearGrid"])
  @smproperty.xml("""
<IntVectorProperty name="BlockType" command="SetBlockType"
  number_of_elements="1" default_values="3">
  <EnumerationDomain name="enum">""" + "\n".join(map(
    lambda kv: '    <Entry value="%d" text="%s"/>' % kv,
    enumerate(VTK_DATASET_TYPE_NAMES))) + """
  </EnumerationDomain>
  <Documentation>
    Select the dataset type of each block, must be homogeneous over the whole multiblock.
  </Documentation>
</IntVectorProperty>""")
  def SetBlockType(self, val):
    #print("Selected Block Type:", val)
    self._blockTypeName = VTK_DATASET_TYPE_NAMES[val]
    self.Modified()
  
  #------------------------------------------------------------------------------------------------#
  
  def RequestData(self, request, iInfoVecTuple, oInfoVec):
    multiblock = vtkMultiBlockDataSet.GetData(iInfoVecTuple[0])
    self._numBlocks = multiblock.GetNumberOfBlocks()
    
    bidx = self._nextToPush
    #print("Pushing block %d of %d downstream" % (bidx + 1, self._numBlocks))
    block = multiblock.GetBlock(bidx)
    BlockType = block.__class__
    assert BlockType.__name__ == self._blockTypeName,\
      "The type of block %d is %s but %s was expected" %\
      (bidx, BlockType.__name__, self._blockTypeName)
    
    fldDat = dsa.WrapDataObject(block).FieldData
    fldDat.append(bidx, "original block index")
    fldDat.append(self._numBlocks, "original number of blocks")
    
    BlockType.GetData(oInfoVec).ShallowCopy(block)
    return 1
