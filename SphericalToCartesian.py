from math import pi as PI, sin, cos

import numpy as np

import vtkmodules
from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import (
  vtkCellArray, vtkRectilinearGrid, vtkUnstructuredGrid, VTK_TRIANGLE as VTK_TRIANGLE)

from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain


TRIANGULATION_MODES = ("ordinary", "fine")

DEG2RAD = PI / 180.

#--------------------------------------------------------------------------------------------------#

@smproxy.filter(label="Spherical to Cartesian")
@smproperty.input(name="Input")
@smdomain.datatype(dataTypes=["vtkRectilinearGrid"], composite_data_supported=True)
class SphericalToCartesian(VTKPythonAlgorithmBase):
  """ Spherical to Cartesian coordinates.
  
  This filter does the following.
  - lon, lat, radius (often constant, can be set as parameter), spherical interpretation --> x, y, z
    (to avoid the point-to-cell-data transformation of the built-in functionality)
  - connect east and west if desired
    (for correct interpolation and topology)
  - triangulate
    (need new cell definition anyway because of the above, TTK cannot work with quads,
    avoid cluttering the pipeline with another filter (*Tetrahedralize*))
  
  Do not check *Spherical Coordinates* in the netCDF source.  
  We assume the latitudes are given in degree north;
  south pole: -90, equator: 0., north pole: +90.  
  Under the *Lighting* properties, you may want to set *Ambient* to 0.75 (close to or equal 1) and
  *Diffuse* to 0, trading depth visualization for clear colormap readability.
  
  NOTE Currently only implemented for 2D input datasets of type `vtkRectilinearGrid`.  
  NOTE We assume the coordinates are sorted ascendingly and the enumeration is row by row.  
  TODO Reuse output vertices and triangles if input grid is not time-dependent.
  """
  
  def __init__ (self):
    VTKPythonAlgorithmBase.__init__(
      self, nInputPorts=1, nOutputPorts=1,
      inputType="vtkRectilinearGrid", outputType="vtkUnstructuredGrid")
    
    self._radius = 90.
    self._connectEastWest = True
    self._triangulationMode = TRIANGULATION_MODES[0]
    self._connectNorth = False
    self._connectSouth = False
    
    self._vertices = None
    self._connectivity = None
  
  #------------------------------------------------------------------------------------------------#
  
  @smproperty.doublevector(name="Radius", default_values=90.)
  #@smdomain.doublerange() # TODO test if negative works
  def SetRadius(self, val):
    # With radius 90 instead of e.g. 1 or 100, two opposite points will have
    # the Euclidean distance 180, like the angle in degree between them.
    self._radius = val
    self.Modified()
  
  
  @smproperty.intvector(name="ConnectEastWest", default_values=1)
  @smdomain.xml("""<BooleanDomain name="bool"/>""")
  def SetConnectEastWest(self, val):
    self._connectEastWest = False if val == 0 else True
    self.Modified()
  
  
  @smproperty.xml("""
<IntVectorProperty
  name="TriangulationMode"
  command="SetTriangulationMode"
  number_of_elements="1"
  default_values="0">
  <EnumerationDomain name="enum">
    <Entry value="0" text="ordinary"/>
    <Entry value="1" text="fine"/>
  </EnumerationDomain>
  <Documentation>
    In any case, the eastern and western cutting line gets connected.
    ordinary:
    Add a diagonal in each quad (numVertsNew = numVertsOld, numCellsNew = numCellsOld*2).
    fine:
    Add the centroid of each quad as new vertex, replacing the quad with four triangles
    (numVertsNew ~= numVertsOld*2, numCellsNew ~= numCellsOld*4).
    The pole is added to the grid.
  </Documentation>
</IntVectorProperty>""")
  def SetTriangulationMode(self, val):
    self._triangulationMode = TRIANGULATION_MODES[val]
    self.Modified()
  
  
  @smproperty.intvector(name="ConnectNorth", default_values=0)
  @smdomain.xml("""<BooleanDomain name="bool"/>""")
  def SetConnectNorth(self, val):
    if val == 0:
      self._connectNorth = False
    else:
      self._connectNorth = True
      #if self._triangulationMode != "fine":
        #print("Warning: can only connect the vertices across the north pole if *Triangulation Mode* is set to *fine*.")
    self.Modified()
  
  
  @smproperty.intvector(name="ConnectSouth", default_values=0)
  @smdomain.xml("""<BooleanDomain name="bool"/>""")
  def SetConnectSouth(self, val):
    if val == 0:
      self._connectSouth = False
    else:
      self._connectSouth = True
      #if self._triangulationMode != "fine":
        #print("Warning: can only connect the vertices across the south pole if *Triangulation Mode* is set to *fine*.")
    self.Modified()
  
  #------------------------------------------------------------------------------------------------#
  
  def project_lon_lat_grid(self, lons, lats):
    radius = self._radius
    vertices = np.empty((len(lons) * len(lats), 3), dtype="f4") # TTK requires f4
    v = 0
    for latInDeg in lats:
      latInRad = latInDeg*DEG2RAD # in [-pi/2, pi/2]
      cosLatRadius = cos(latInRad) * radius # for x and y
      sinLatRadius = sin(latInRad) * radius # for z
      for lonInDeg in lons:
        lonInRad = lonInDeg*DEG2RAD # in [0, 2*pi)
        vertices[v, 0] = cos(lonInRad) * cosLatRadius
        vertices[v, 1] = sin(lonInRad) * cosLatRadius
        vertices[v, 2] = sinLatRadius
        v += 1
    return vertices
  
  #------------------------------------------------------------------------------------------------#
  
  def set_vertices_and_cells(self, oDataset):
    oDataset.Points = self._vertices
    conity = dsa.numpyTovtkDataArray(
      self._connectivity, array_type=vtkmodules.util.vtkConstants.VTK_ID_TYPE)
    cellArray = vtkCellArray()
    cellArray.SetCells(len(self._connectivity) // 4, conity)
    oDataset.VTKObject.SetCells(VTK_TRIANGLE, cellArray)
  
  #------------------------------------------------------------------------------------------------#
  
  def set_triangle_pair(self, idx, v00, v01, v10, v11):
    conity = self._connectivity
    conity[idx] = 3
    conity[idx + 1] = v00
    conity[idx + 2] = v10
    conity[idx + 3] = v11
    idx += 4
    conity[idx] = 3
    conity[idx + 1] = v00
    conity[idx + 2] = v11
    conity[idx + 3] = v01
    return idx + 4
  
  #------------------------------------------------------------------------------------------------#
  
  def execute_ordinary(self, iDataset, oDataset, lons, lats):
    self._vertices = self.project_lon_lat_grid(lons, lats)
    
    nx = len(lons)
    ny = len(lats)
    connectEastWest = self._connectEastWest
    numTriangles = (ny - 1) * nx * 2 if connectEastWest else (ny - 1) * (nx - 1) * 2
    self._connectivity = np.empty((numTriangles*4), dtype=int) # n p q r for each cell
    maxj = nx - 1
    conIdx = 0
    for i in range(ny - 1):
      offi = i*nx
      for j in range(maxj):
        v00 = offi + j
        v10 = v00 + nx
        conIdx = self.set_triangle_pair(conIdx, v00, v00 + 1, v10, v10 + 1)
      if connectEastWest:
        v00 = offi + maxj
        conIdx = self.set_triangle_pair(conIdx, v00, offi, v00 + nx, v00 + 1)
    
    self.set_vertices_and_cells(oDataset)
    #for name, values in iDataset.PointData.items():
      #oDataset.PointData.append(values, name)
    # there is no `items` method (as of 2020-03-20)
    iPointData = iDataset.PointData
    oPointData = oDataset.PointData
    for name in iPointData.keys():
      oPointData.append(iPointData[name], name)
    return 1
  
  #------------------------------------------------------------------------------------------------#
  
  def set_triangle_quadruplet(self, idx, v00, v01, v10, v11, vCen):
    conity = self._connectivity
    conity[idx] = 3
    conity[idx + 1] = v00
    conity[idx + 2] = vCen
    conity[idx + 3] = v01
    idx += 4
    conity[idx] = 3
    conity[idx + 1] = v01
    conity[idx + 2] = vCen
    conity[idx + 3] = v11
    idx += 4
    conity[idx] = 3
    conity[idx + 1] = v11
    conity[idx + 2] = vCen
    conity[idx + 3] = v10
    idx += 4
    conity[idx] = 3
    conity[idx + 1] = v10
    conity[idx + 2] = vCen
    conity[idx + 3] = v00
    return idx + 4
  
  
  def set_triangle(self, idx, p, q, r):
    conity = self._connectivity
    conity[idx] = 3
    conity[idx + 1] = p
    conity[idx + 2] = q
    conity[idx + 3] = r
    return idx + 4
  
  #------------------------------------------------------------------------------------------------#
  
  def execute_fine(self, iDataset, oDataset, lonsGiven, latsGiven):
    nxGiven = len(lonsGiven)
    nyGiven = len(latsGiven)
    maxjGiven = nxGiven - 1
    maxiGiven = nyGiven - 1
    
    connectEastWest = self._connectEastWest
    nxExtra = nxGiven if connectEastWest else nxGiven - 1
    
    lonsExtra = np.empty((nxExtra), dtype=lonsGiven.dtype)
    for j in range(maxjGiven):
      lonsExtra[j] = (lonsGiven[j] + lonsGiven[j+1]) / 2.
    if connectEastWest:
      lonsExtra[maxjGiven] = (lonsGiven[maxjGiven] + lonsGiven[0] + 360.) / 2.
    
    latsExtra = np.empty((nyGiven-1), dtype=latsGiven.dtype)
    for i in range(maxiGiven):
      latsExtra[i] = (latsGiven[i] + latsGiven[i+1]) / 2.
    
    
    # Create the vertices
    
    verticesGiven = self.project_lon_lat_grid(lonsGiven, latsGiven)
    verticesExtra = self.project_lon_lat_grid(lonsExtra, latsExtra)
    verticesTempList = [verticesGiven, verticesExtra]
    if self._connectSouth:
      verticesTempList.append(np.array([(0., 0., -self._radius)], "f4"))
    if self._connectNorth:
      verticesTempList.append(np.array([(0., 0.,  self._radius)], "f4"))
    self._vertices = np.concatenate(verticesTempList)
    
    nvAll = len(self._vertices)
    vNorth = nvAll - 1 if self._connectNorth else nvAll
    vSouth = vNorth - 1
    
    
    # Create the cells
    
    numTriangles = (nyGiven - 1) * (nxGiven - 1) * 4 # "inner" cells
    if connectEastWest:
      numTriangles += (nyGiven - 1) * 4 # "glue" cells
    if self._connectSouth:
      numTriangles += nxExtra
    if self._connectNorth:
      numTriangles += nxExtra
    self._connectivity = np.empty((numTriangles*4), dtype=int) # n p q r for each cell
    
    nvGiven = nyGiven * nxGiven
    conIdx = 0
    offiOut = nvGiven
    for i in range(maxiGiven):
      offi = i*nxGiven
      for j in range(maxjGiven):
        v00 = offi + j
        v10 = v00 + nxGiven
        conIdx = self.set_triangle_quadruplet(
          conIdx, v00, v00 + 1, v10, v10 + 1, offiOut + j)
      if connectEastWest:
        v00 = offi + maxjGiven
        conIdx = self.set_triangle_quadruplet(
          conIdx, v00, offi, v00 + nxGiven, offi + nxGiven, v00 + nvGiven)
      offiOut += nxExtra
    
    if self._connectSouth:
      for v in range(0, nxGiven - 1):
        conIdx = self.set_triangle(conIdx, v, vSouth, v + 1)
      if connectEastWest:
        conIdx = self.set_triangle(conIdx, nxGiven - 1, vSouth, 0)
    
    if self._connectNorth:
      for v in range(nvGiven - nxGiven, nvGiven - 1):
        conIdx = self.set_triangle(conIdx, v, vNorth, v + 1)
      if connectEastWest:
        conIdx = self.set_triangle(conIdx, nvGiven - 1, vNorth, nvGiven - nxGiven)
    
    
    # Set the data
    
    self.set_vertices_and_cells(oDataset)
    iPointData = iDataset.PointData
    oPointData = oDataset.PointData
    
    # compute the value at the pole by linear extrapolation from the two previous rows (see below)
    latS0 = latsGiven[1]
    latS1 = latsGiven[0]
    offiS0 = nxGiven
    offiS1 = 0
    extraFacS = (-90. - latS0) / (latS1 - latS0)
    latN0 = latsGiven[nyGiven-2]
    latN1 = latsGiven[nyGiven-1]
    offiN0 = (nyGiven-2) * nxGiven
    offiN1 = (nyGiven-1) * nxGiven
    extraFacN = (90. - latN0) / (latN1 - latN0)
    
    for name in iPointData.keys():
      iData = iPointData[name] # may or may not be scalars
      oShape = list(iData.shape) # tuple is immutable
      oShape[0] = nvAll
      # NOTE Because of the interpolation, the output data type should always be f4 or f8;
      # but what about e.g. index fields?
      # NOTE String data (e.g. point labels) need special treatment (e.g. set empty string)
      oData = np.empty(oShape, iData.dtype)
      oData[:nvGiven] = iData # copy the given data
      
      # We interpolate the data at the extra vertices using equal contribution from all connected
      # given vertices -- no weighting by distance is applied.
      # (The two given vertices farther from the equator are a little closer to the extra vertex but
      # also represent a smaller (cell) area of the domain.)
      offiOut = nvGiven
      for i in range(maxiGiven):
        offi = i*nxGiven
        for j in range(maxjGiven):
          v00 = offi + j
          v10 = v00 + nxGiven
          oData[offiOut+j] = (iData[v00] + iData[v00+1] + iData[v10] + iData[v10+1]) / 4.
        if connectEastWest:
          v00 = offi + maxjGiven
          v01 = offi
          oData[v00+nvGiven] = (iData[v00] +iData[v01] +iData[v00+nxGiven] +iData[v01+nxGiven]) / 4.
        offiOut += nxExtra
      
      if self._connectSouth:
        poleData = np.zeros_like(iData[0]) # get shape (number of components) and data type
        for j in range(nxGiven):
          dat0 = iData[offiS0 + j]
          dat1 = iData[offiS1 + j]
          poleData += dat0 + (dat1 - dat0) * extraFacS
        oData[vSouth] = poleData / nxGiven
      if self._connectNorth:
        poleData = np.zeros_like(iData[0]) # get shape (number of components) and data type
        for j in range(nxGiven):
          dat0 = iData[offiN0 + j]
          dat1 = iData[offiN1 + j]
          poleData += dat0 + (dat1 - dat0) * extraFacN
        oData[vNorth] = poleData / nxGiven
      
      oPointData.append(oData, name)
      
    return 1
  
  #------------------------------------------------------------------------------------------------#
  
  def RequestData(self, request, iInfoVecTuple, oInfoVec):
    iDataset = dsa.WrapDataObject(vtkRectilinearGrid.GetData(iInfoVecTuple[0]))
    oDataset = dsa.WrapDataObject(vtkUnstructuredGrid.GetData(oInfoVec))
    lons = dsa.vtkDataArrayToVTKArray(iDataset.GetXCoordinates())
    lats = dsa.vtkDataArrayToVTKArray(iDataset.GetYCoordinates())
    heights = dsa.vtkDataArrayToVTKArray(iDataset.GetZCoordinates())
    if heights[0] != 0. or len(heights) > 1:
      print("Warning: ignoring height values:", heights)
    
    fieldData = oDataset.FieldData
    if "radius" in fieldData:
      print("Warning: overwriting fieldData.radius from %f to %f" % (fieldData["radius"], self._radius))
    #fieldData["radius"] = self._radius # dict interface only for getting not setting
    fieldData.append(self._radius, "radius")
    
    if self._triangulationMode == "fine":
      return self.execute_fine(iDataset, oDataset, lons, lats)
    else:
      return self.execute_ordinary(iDataset, oDataset, lons, lats)
  
