import numpy as np

import vtkmodules
from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import vtkCellArray, vtkPointSet, vtkPolyData

from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain


@smproxy.filter(label="Polyline Per Cluster")
@smproperty.input(name="Points")
@smdomain.datatype(dataTypes=["vtkPointSet"], composite_data_supported=True)
class PolylinePerCluster(VTKPythonAlgorithmBase):
  """ For each cluster with more than one point, create a polyline through its points.
  
  To order the points, data must be selected that acts as parametrization of the polyline domain.
  """
  
  def __init__ (self):
    VTKPythonAlgorithmBase.__init__(self, nInputPorts=1, nOutputPorts=1, outputType="vtkPolyData")
    self._attrNameClusterId = ""
    self._attrNameParametrization = ""
    self._minPts = 4
  
  #------------------------------------------------------------------------------------------------#
  
  @smproperty.xml("""
<StringVectorProperty name="ClusterId" number_of_elements="1" command="SetClusterId">
  <ArrayListDomain name="array_list" default_values="0">
    <RequiredProperties><Property function="Input" name="Points"/></RequiredProperties>
  </ArrayListDomain>
  <Documentation>Select the attribute that defines the cluster ID of the points.</Documentation>
</StringVectorProperty>""")
  def SetClusterId(self, val):
    self._attrNameClusterId = val
    self.Modified()
  
  
  @smproperty.xml("""
<StringVectorProperty name="Parametrization" number_of_elements="1" command="SetParametrization">
  <ArrayListDomain name="array_list" default_values="3">
    <RequiredProperties><Property function="Input" name="Points"/></RequiredProperties>
  </ArrayListDomain>
  <Documentation>Select the attribute that defines the parametrization of the polylines.</Documentation>
</StringVectorProperty>""")
  def SetParametrization(self, val):
    self._attrNameParametrization = val
    self.Modified()
  
  
  @smproperty.intvector(name="MinimumPoints", default_values=4)
  @smdomain.intrange(min=2)
  def SetMinimumPoints(self, val):
    """ Required minimum number of samples per polyline """
    self._minPts = val
    self.Modified()
  
  #------------------------------------------------------------------------------------------------#
  
  def execute(self, iPointSet, oPolyData):
    if self._attrNameClusterId == "" or self._attrNameParametrization == "":
      return 0
    
    clusterIds = iPointSet.PointData[self._attrNameClusterId]
    params = iPointSet.PointData[self._attrNameParametrization]
    
    clIdMin = np.min(clusterIds)
    clIdMax = np.max(clusterIds)
    polylines = tuple([] for _ in range(clIdMax - clIdMin + 1))
    for ptId, clId in enumerate(clusterIds):
      polylines[clId - clIdMin].append(ptId)
    
    minPts = self._minPts
    get_param = lambda ptId: params[ptId]
    vtkCells = vtkCellArray()
    for polyline in polylines:
      n = len(polyline)
      if n < minPts:
        continue
      polyline.sort(key=get_param)
      # Make sure the parameters are unique
      # We have no way of prioritizing or aggregating points here but we do not want to stop the
      # pipeline, so we just delete the competing points(from the polyline, not the point set) and
      # print a warning. The gap may be interpolated later.
      toDelete = []
      a = params[polyline[0]]
      ambiguous = False
      for i in range(1, n):
        b = params[polyline[i]]
        if b == a:
          # this approach also works for more than two identical values in a row
          if len(toDelete) == 0 or toDelete[-1] != i - 1:
            toDelete.append(i - 1)
          toDelete.append(i)
        a = b
      numDel = len(toDelete)
      if numDel > 0:
        print("[PolylinePerCluster] Warning: must ignore %d points because their parametrization values are not unique" % numDel)
        if n - numDel < minPts:
          continue
        for i in reversed(toDelete):
          polyline.pop(i)
      vtkCells.InsertNextCell(n - numDel, polyline)
    oPolyData.SetLines(vtkCells)
    
    # copy the points and point data
    oPolyData.Points = iPointSet.Points
    iPointData = iPointSet.PointData
    oPointData = oPolyData.PointData
    for name in iPointData.keys(): # there is no .items() :-(
      oPointData.append(iPointData[name], name)
    
    #print("<-- END", type(self).__name__)
    return 1
  
  #------------------------------------------------------------------------------------------------#
  
  def RequestData(self, request, iInfoVecTuple, oInfoVec):
    #print("--> BEGIN", type(self).__name__)
    iPointSet = dsa.WrapDataObject(vtkPointSet.GetData(iInfoVecTuple[0]))
    oPolyData = dsa.WrapDataObject(vtkPolyData.GetData(oInfoVec))
    return self.execute(iPointSet, oPolyData)
