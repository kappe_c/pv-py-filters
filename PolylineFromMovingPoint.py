from math import sqrt

import numpy as np

import vtkmodules
from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import vtkCellArray, vtkPointSet, vtkPolyData

from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain

from aaaown.PointTrackerSimple import PointTrackerSimple as PointTracker
#from aaaown.PointTrackerComplex import PointTrackerComplex as PointTracker



@smproxy.filter(label='PolylineFromMovingPoint')
@smproperty.input(name='Points')
@smdomain.datatype(dataTypes=['vtkPointSet'], composite_data_supported=True)
class PolylineFromMovingPoint(VTKPythonAlgorithmBase):
	'''Create a polyline for each track of points that can be found in the data.
	
	To order the points, data must be selected that acts as parametrization of the polyline domain.
	
	In February 2022, this code is revived from the dead (May 2020) as comparison for a paper.
	'''
	def __init__ (self):
		VTKPythonAlgorithmBase.__init__(
			self, nInputPorts=1, nOutputPorts=1, outputType='vtkPolyData')
		self._attrNameParametrization = ''
		self._maxAllowedDist = 0.
		self._minPts = 4
		self._attrNameExtra = ''
		self._extraWeightInPercent = 0.
	
	#----------------------------------------------------------------------------------------------#
	
	@smproperty.xml('''
	<StringVectorProperty name="Parametrization" number_of_elements="1" command="SetParametrization">
		<ArrayListDomain name="array_list" default_values="0">
			<RequiredProperties><Property function="Input" name="Points"/></RequiredProperties>
		</ArrayListDomain>
		<Documentation>Select the attribute that defines the parametrization of the polylines.</Documentation>
	</StringVectorProperty>''')
	def SetParametrization(self, val):
		self._attrNameParametrization = val
		self.Modified()
	
	
	@smproperty.doublevector(name='MaximumDistance', default_values=0.)
	@smdomain.doublerange(min=0.)
	def SetMaximumDistance(self, val):
		'''Maximum allowed distance between subsequent samples; 0 means no restriction'''
		self._maxAllowedDist = val
		self.Modified()
	
	
	@smproperty.intvector(name='MinimumPoints', default_values=4)
	@smdomain.intrange(min=2)
	def SetMinimumPoints(self, val):
		'''Required minimum number of samples per polyline'''
		self._minPts = val
		self.Modified()
	
	
	@smproperty.xml('''
	<StringVectorProperty name="ScalarAttribute" number_of_elements="1" command="SetScalarAttribute">
		<ArrayListDomain name="array_list" default_values="0">
			<RequiredProperties><Property function="Input" name="Points"/></RequiredProperties>
		</ArrayListDomain>
		<Documentation>Select the scalar attribute to consider.</Documentation>
	</StringVectorProperty>''')
	def SetScalarAttribute(self, val):
		self._attrNameExtra = val
		self.Modified()
	
	@smproperty.doublevector(name='ScalarWeight', default_values=0.)
	@smdomain.doublerange(min=0., max=100.)
	def SetScalarWeight(self, val):
		self._extraWeightInPercent = val
		self.Modified()
	
	#----------------------------------------------------------------------------------------------#
	
	def execute(self, iPointSet, oPolyData):
		if self._attrNameParametrization == '':
			return 0
		
		params = iPointSet.PointData[self._attrNameParametrization]
		extras = iPointSet.PointData[self._attrNameExtra]
		pts3d = iPointSet.Points
		
		if self._extraWeightInPercent != 0.:
			print('Warning: not implemented yet: scalar attribute cannot be considered in distance')
		
		tracker = PointTracker(
			lambda a, b: sqrt(((b-a)**2).sum()), self._maxAllowedDist or float('inf'))
		
		# check if the points are already sorted by time
		notSorted = False
		prevTimeVal = params[0]
		timeStepEndIndices = []
		for p, time in enumerate(params):
			if time < prevTimeVal:
				notSorted = True
				break
			elif time > prevTimeVal:
				timeStepEndIndices.append(p)
				prevTimeVal = time
		timeStepEndIndices.append(len(params))
		
		if notSorted:
			print('Error: not implemented yet: points are not sorted w.r.t. parametrization')
			oPolyData.ShallowCopy(iPointSet)
			return 1
			# must sort all the point data accordingly
			#todo.sort(key=lambda ptId: params[ptId])
		
		numT = len(timeStepEndIndices)
		#print('Found %d time steps'%numT)
		
		tracker.first_time_step(pts3d[:timeStepEndIndices[0]])
		
		for t in range(numT-1):
			beg = timeStepEndIndices[t]
			end = timeStepEndIndices[t+1]
			tracker.next_time_step(pts3d[beg:end])
		
		minPts = self._minPts
		vtkCells = vtkCellArray()
		for track in filter(lambda a: len(a)>=minPts, tracker.get_tracks()):
		 	vtkCells.InsertNextCell(len(track), track)
		oPolyData.SetLines(vtkCells)
		
		# copy the points and point data
		oPolyData.Points = pts3d
		iPointData = iPointSet.PointData
		oPointData = oPolyData.PointData
		for name in iPointData.keys(): # there is no .items() :-(
		  oPointData.append(iPointData[name], name)
		
		#print("<-- END", type(self).__name__)
		return 1
	
	#----------------------------------------------------------------------------------------------#
	
	def RequestData(self, request, iInfoVecTuple, oInfoVec):
		#print("--> BEGIN", type(self).__name__)
		iPointSet = dsa.WrapDataObject(vtkPointSet.GetData(iInfoVecTuple[0]))
		oPolyData = dsa.WrapDataObject(vtkPolyData.GetData(oInfoVec))
		return self.execute(iPointSet, oPolyData)
	
