import numpy as np

from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import vtkDataObject, vtkUnstructuredGrid
from vtkmodules.vtkCommonExecutionModel import vtkStreamingDemandDrivenPipeline
from vtk import vtkStaticPointLocator

from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain


IO_TYPE_STR = "vtkUnstructuredGrid"
IO_TYPE_CLS = vtkUnstructuredGrid

#--------------------------------------------------------------------------------------------------#

@smproxy.filter(label="CorrelationMap")
@smproperty.input(name="Domain", port_index=1)
@smdomain.datatype(dataTypes=[IO_TYPE_STR])
@smproperty.input(name="Constraints", port_index=0)
@smdomain.datatype(dataTypes=[IO_TYPE_STR])
class CorrelationMap(VTKPythonAlgorithmBase):
  """ Create a correlation map for the vertices of the time-dependent "Domain" input field
  
  w.r.t. one or more points given as "Constraints".
  NOTE We assume that the mesh of the input field is constant over time.
  """
  
  def __init__ (self):
    VTKPythonAlgorithmBase.__init__(
      self, nInputPorts=2, nOutputPorts=1, outputType=IO_TYPE_STR)
    #self._inputTypes = [IO_TYPE_STR, IO_TYPE_STR]
    self._attrName = ""
    self._tWinExtInOneDir = 12 # time window extension in one direction
  
  #------------------------------------------------------------------------------------------------#

  #def FillInputPortInformation(self, port, info):
    #""" Overriding this method to handle multiple inputs. """
    #info.Set(vtkDataObject.DATA_TYPE_NAME(), self._inputTypes[port])
    #return 1
  
  #------------------------------------------------------------------------------------------------#
  
  @smproperty.xml("""
<StringVectorProperty name="Scalars" number_of_elements="1" command="SetScalars">
  <ArrayListDomain name="array_list" default_values="0">
    <RequiredProperties>
      <Property function="Input" name="Domain" />
    </RequiredProperties>
  </ArrayListDomain>
  <Documentation>Select the scalar variable to consider.</Documentation>
</StringVectorProperty>""")
  def SetScalars(self, val):
    #print("Scalar variable:", val)
    self._attrName = val
    self.Modified()
  
  
  @smproperty.intvector(name="HalfWindowSize", default_values=12)
  @smdomain.intrange(min=1)
  #@smproperty.xml("""<Documentation>Number of time steps to consider before and after the current one. The size of the time window is this value times two plus one.</Documentation>""")
  def SetTWinExtInOneDir(self, val):
    #print("Half window size:", val)
    self._tWinExtInOneDir = val
    self.Modified()
  
  #------------------------------------------------------------------------------------------------#
  
  def get_time_series_for_vertices(self, curScalars, fldAlgo, tVals, tKeyCur):
    attrName = self._attrName
    tWinExtInOneDir = self._tWinExtInOneDir
    tWinSize = tWinExtInOneDir*2 + 1
    v2t2val = np.empty((len(curScalars), tWinSize), dtype=curScalars.dtype)
    
    # get the data that is currently loaded
    for v, t2val in enumerate(v2t2val):
      v2t2val[v][tWinExtInOneDir] = curScalars[v]
    
    # get the remaining data from the input
    tOff = tKeyCur - tWinExtInOneDir
    for t in range(tWinSize):
      if t != tWinExtInOneDir:
        fldAlgo.UpdateTimeStep(tVals[t + tOff])
        scalars = dsa.WrapDataObject(fldAlgo.GetOutputDataObject(0)).PointData[attrName]
        for v, t2val in enumerate(v2t2val):
          v2t2val[v][t] = scalars[v]
    
    fldAlgo.UpdateTimeStep(tVals[tKeyCur]) # reset to the current time
    return v2t2val
  
  #------------------------------------------------------------------------------------------------#
  
  def execute(self, iPts, iFld, oFld):
    points = iPts.Points # numpy array (numPts, 3)
    numPts = len(points)
    if numPts == 0:
      print("Error: input (port 0) points dataset does not contain any points")
      return 0
    if numPts > 10:
      print("Warning: input (port 0) points dataset contains rather a lot of points (%d)" % numPts)
    
    tWinExtInOneDir = self._tWinExtInOneDir
    iFldTimeVals = self.GetInputInformation(1, 0).Get(vtkStreamingDemandDrivenPipeline.TIME_STEPS())
    iFldTimeKeyMin = tWinExtInOneDir
    iFldTimeKeyMax = len(iFldTimeVals) - 1 - tWinExtInOneDir
    # It seems there is no possibility to get the current time *step* (index/key),
    # so one has to find it inefficiently by searching the list of *values*.
    iFldTimeValCur = iFld.GetInformation().Get(vtkDataObject.DATA_TIME_STEP())
    iFldTimeKeyCur = iFldTimeVals.index(iFldTimeValCur)
    if iFldTimeKeyCur < iFldTimeKeyMin or iFldTimeKeyCur > iFldTimeKeyMax:
      print("Warning: the current time step (%d) is too low or too high to compute correlations; the time window size set to %d+1+%d; the earliest computable time step is %d the latest is %d" % (iFldTimeKeyCur, tWinExtInOneDir, tWinExtInOneDir, iFldTimeKeyMin, iFldTimeKeyMax))
      return 0 # NOTE empty output (as set by default) but no error may be convenient
    
    
    # We map the input points to the closest input field vertices (aka input field sample positions)
    samplePosLocator = vtkStaticPointLocator()
    samplePosLocator.SetDataSet(iFld.VTKObject)
    # There should be no duplicate vertices
    vSet = set()
    vsSelected = []
    for point in points:
      v = samplePosLocator.FindClosestPoint(point)
      if v not in vSet:
        vSet.add(v)
        vsSelected.append(v)
    
    numVsSelected = len(vsSelected)
    if numVsSelected != numPts:
      print("By mapping to the closest input field vertices, the number of points to consider has shrunk from %d to %d" % (numPts, numVsSelected))
    
    iScalars = iFld.PointData[self._attrName]
    v2t2val = self.get_time_series_for_vertices(
      iScalars, self.GetInputAlgorithm(1, 0), iFldTimeVals, iFldTimeKeyCur)
    
    oScalars = None
    oAttrName = "corr"
    # Often np.corrcoef is used to calculate a complete correlation matrix for
    # an array of input arrays. However, we only want one row/column of this matrix, so
    # we use the second variant of the function that takes just two input arrays.
    # For consistency reasons (presumably), this variant also returns a matrix;
    # this is a symmetric 2x2 matrix with ones on the diagonal and the actual result at 0,1 and 1,0.
    if numVsSelected == 1:
      oScalars = np.empty(len(iScalars), dtype="f4")
      vSel = vsSelected[0]
      oScalars[vSel] = 1.
      vSel_t2val = v2t2val[vSel]
      for v, t2val in enumerate(v2t2val):
        if v != vSel:
          oScalars[v] = np.corrcoef(vSel_t2val, t2val)[0, 1]
    else:
      # compute the Root Mean Square
      oAttrName += "RMS"
      oScalarsAsF8 = np.zeros(len(iScalars))
      for vSel in vsSelected:
        oScalarsAsF8[vSel] += 1.
        vSel_t2val = v2t2val[vSel]
        for v, t2val in enumerate(v2t2val):
          if v != vSel:
            oScalarsAsF8[v] += np.corrcoef(vSel_t2val, t2val)[0, 1]**2
      oScalarsAsF8 /= numVsSelected
      oScalars = np.sqrt(oScalarsAsF8).astype("f4")
    
    oFld.PointData.append(oScalars, oAttrName)
    oFld.PointData.SetActiveScalars(oAttrName)
    return 1
  
  #------------------------------------------------------------------------------------------------#
  
  def RequestData(self, request, iInfoVecTuple, oInfoVec):
    print("-- %s.RequestData" % type(self).__name__)
    if len(iInfoVecTuple) < 2:
      print("Only %d of 2 inputs connected" % len(iInfoVecTuple))
      return 0
    ptsInfo = iInfoVecTuple[0]
    fldInfo = iInfoVecTuple[1]
    #print("points: iInfoVecTuple[0]:", ptsInfo)
    #print("field : iInfoVecTuple[1]:", fldInfo)
    iFld = IO_TYPE_CLS.GetData(fldInfo)
    oFld = IO_TYPE_CLS.GetData(oInfoVec)
    oFld.ShallowCopy(iFld) # does not return anything
    wrap = dsa.WrapDataObject
    return self.execute(wrap(IO_TYPE_CLS.GetData(ptsInfo)), wrap(iFld), wrap(oFld))
  
