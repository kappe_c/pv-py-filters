from math import sqrt

import numpy as np

from sklearn.cluster import DBSCAN

import vtkmodules
from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import \
  vtkDataObject, vtkMultiBlockDataSet, vtkPointSet#, vtkUnstructuredGrid

from paraview import servermanager
from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain

from aaaown import pv_spherical_domain
#from aaaown.average_angle import average_angle

#--------------------------------------------------------------------------------------------------#

@smproxy.filter(label="Density-Based Clustering")
@smproperty.input(name="Points")
@smdomain.datatype(dataTypes=["vtkPointSet"], composite_data_supported=True)
class DensityBasedClustering(VTKPythonAlgorithmBase):
  """ Density-Based Clustering.
  
  ... of points in 3D Euclidean space, also (optionally) considering one scalar attribute.
  The first output contains the clustered points with the computed cluster ID,
  the second output contains the cluster means,
  the third output contains the points that have been classified as noise.
  
  Notice that currently no cell information is carried over into the output.
  """
  
  def __init__ (self):
    VTKPythonAlgorithmBase.__init__(
      self, nInputPorts=1, nOutputPorts=3)#, outputType="vtkPointSet")
    
    self._attrName = ""
    # the typical DBSCAN parameters
    self._eps = 0.5
    self._minPts = 5 # "tetrahedron around the current point"
    
    self._scalarWeightInPercent = 0.
    self._ambiguousBlocks = False
    
    self._compMeans = True # user may skip this for speed
    self._onSphere = False # only considered for the means
    
    self._numClusters = 0 # over all blocks (will be lower if _ambiguousBlocks is true)
    #self._dsTypeNameConcrete = "" # will be set to input dataset type
    self._lastRequest = None # to detect requests stemming from the same vtkMultiBlockDataSet
    self._blockCounter = 0 # number of datasets that have been processed
  
  
  #def FillOutputPortInformation(self, port, info):
    ##print("FillOutputPortInformation port", port, " type", self._dsTypeNameConcrete)
    ##info.Set(vtkDataObject.DATA_TYPE_NAME(), self._dsTypeNameConcrete)
    #print("FillOutputPortInformation(port=%d)" % port)
    #if port == 0:
      #info.Set(vtkDataObject.DATA_TYPE_NAME(), "vtkPointSet")
      #info.Set(vtkDataObject.FIELD_NAME(), "ClusteredPoints")
    #elif port == 1:
      #info.Set(vtkDataObject.DATA_TYPE_NAME(), "vtkPointSet")
      #info.Set(vtkDataObject.FIELD_NAME(), "Noise")
    #return 1
  
  #------------------------------------------------------------------------------------------------#
  
  @smproperty.xml("""
<StringVectorProperty name="ScalarAttribute" number_of_elements="1" command="SetScalarAttribute">
  <ArrayListDomain name="array_list" default_values="0">
    <RequiredProperties><Property function="Input" name="Points"/></RequiredProperties>
  </ArrayListDomain>
  <Documentation>Select the scalar attribute to consider.</Documentation>
</StringVectorProperty>""")
  def SetScalarAttribute(self, val):
    self._attrName = val
    self.Modified()
  
  
  @smproperty.intvector(name="MinimumPoints", default_values=5)
  @smdomain.intrange(min=2, max=99)
  def SetMinimumPoints(self, val):
    self._minPts = val
    self.Modified()
  
  
  @smproperty.doublevector(name="Epsilon", default_values=0.5)
  @smdomain.doublerange(min=0.)
  def SetEpsilon(self, val):
    """ If the scalar weight is not set to 100, epsilon refers to the range of spatial distances """
    self._eps = val
    self.Modified()
  
  
  @smproperty.doublevector(name="ScalarWeight", default_values=0.)
  @smdomain.doublerange(min=0., max=100.)
  def SetScalarWeight(self, val):
    self._scalarWeightInPercent = val
    self.Modified()
  
  
  @smproperty.intvector(name="OnSphere", default_values=0)
  @smdomain.xml("""<BooleanDomain name="bool"/>""")
  def SetOnSphere(self, val):
    """ Do the input (Cartesian!) points all lie on a sphere?
    
    And should the output means therefore also all lie on a sphere with the same radius?
    """
    self._onSphere = False if val == 0 else True
    self.Modified()
  
  
  @smproperty.intvector(name="AmbiguousBlocks", default_values=0)
  @smdomain.xml("""<BooleanDomain name="bool"/>""")
  def SetAmbiguousBlocks(self, val):
    """ Reset the cluster IDs for each block (starting at 0 again)
    
    in case of a multiblock dataset, thus making them ambiguous after e.g. a MergeBlocks filter.
    """
    self._ambiguousBlocks = False if val == 0 else True
    self.Modified()
  
  #------------------------------------------------------------------------------------------------#
  
  def get_points_for_cling(self, iDataSet):
    wScalar = self._scalarWeightInPercent / 100.
    spatials = iDataSet.Points
    scalars = iDataSet.PointData[self._attrName]
    
    if type(scalars) == dsa.VTKNoneArray or wScalar == 0.:
      #print(type(self).__name__, "using only the spatial data")
      return spatials
    
    if wScalar == 1.:
      #print(type(self).__name__, "using only the scalar data")
      return scalars
    
    # compute a scaling factor to apply to the scalars for comparability
    spatialRange = np.ptp(spatials, axis=0) # result has 3 components
    scalarRange = np.ptp(scalars)
    spatialRangeRms = sqrt((spatialRange**2).mean())
    # This number is the length of the diagonal of the bounding box divided by sqrt(3)
    # (1/sqrt(3) = 0.58).
    # This is kind of a compromise between the arithmetic mean of the three edges, and the maximum.
    
    #if np.isnan(scalarRange).any() or np.isnan(scalarRange):
      #print('spatialRange, scalarRange, spatialRangeRms:',
            #spatialRange, scalarRange, spatialRangeRms)
      #print('spatials, scalars:')
      #for pt, sca in zip(spatials, scalars):
        #print('%7f %7f %7f %7f' % (pt[0], pt[1], pt[2], sca))
    
    closeToZero = 1e-9
    if scalarRange < closeToZero:
      print('Warning: scalar range is (close to) zero: %e. Setting it to 1e-9 for division)' % scalarRange)
      scalarRange = closeToZero
    
    points = np.empty(shape=(len(spatials), 4), dtype='f4')
    points[:, 0:3] = spatials # copy
    points[:, 3] = scalars # copy
    points[:, 0:3] *= 1. - wScalar # not cubic root or sth like that
    points[:, 3] *= wScalar * spatialRangeRms / scalarRange
    
    return points
  
  #------------------------------------------------------------------------------------------------#
  
  def execute(self, iDataSet, oClusteredSet, oMeanSet, oNoiseSet):
    numPts = len(iDataSet.Points)
    if numPts == 0:
      print('Point set is empty')
      return 1
    if numPts == 1:
      print('Point set contains only one point') # just info, should still work
    
    pointsForCling = self.get_points_for_cling(iDataSet)
    ids = None
    try:
      ids = DBSCAN(self._eps, min_samples=self._minPts).fit_predict(pointsForCling)
    except BaseException as e:
      print(e)
      print('-> Every point is classified as noise so that the pipeline can flow on for now')
      ids = np.full((numPts), -1, int)
      #print('Raw points (and scalars):')
      #print(np.c_[iDataSet.Points, iDataSet.PointData[self._attrName]])
      #print('Weighted points (and scalars):')
      #print(pointsForCling)
    
    iPointData = iDataSet.PointData
    
    noise = ids == -1 ## boolean array --> mask
    oNoiseSet.Points = iDataSet.Points[noise]
    oNoiseData = oNoiseSet.PointData
    for name in iPointData.keys(): # there is no .items() :-(
      oNoiseData.append(iPointData[name][noise], name)
    
    minClu = 0 # minimum cluster index (in this block) that does not mean *noise*
    numClustersInThisBlock = np.max(ids) + 1
    if self._ambiguousBlocks:
      self._numClusters = max(self._numClusters, numClustersInThisBlock)
    else:
      minClu = self._numClusters
      ids += minClu
      self._numClusters += numClustersInThisBlock
      
    notNoise = np.invert(noise, noise)
    clusteredPositions = iDataSet.Points[notNoise]
    cluIds = ids[notNoise]
    
    cluAttrName = "cluster"
    numExistingClusterings = 1
    while type(iDataSet.PointData.GetArray(cluAttrName)) != dsa.VTKNoneArray:
      numExistingClusterings += 1
      cluAttrName = "cluster%d" % numExistingClusterings
    
    oClusteredSet.Points = clusteredPositions
    oPointData = oClusteredSet.PointData
    oPointData.append(cluIds, cluAttrName)
    for name in iPointData.keys(): # there is no .items() :-(
      oPointData.append(iPointData[name][notNoise], name)
    
    endClu = minClu + numClustersInThisBlock
    #print("Cluster indices (in this block) are in [%d, %d)" % (minClu, endClu))
    
    if not self._compMeans:
      return 1
    
    meanPositions = None
    oMeanData = oMeanSet.PointData
    if False:#self._onSphere:
      r = pv_spherical_domain.get_radius(iDataSet)
      iSphericals = pv_spherical_domain.cartesian_to_spherical(clusteredPositions)
      oSphericals = np.empty((numClustersInThisBlock, 2))
      for i in range(numClustersInThisBlock):
        positions = iSphericals[cluIds == minClu + i]
        oSphericals[i][0] = average_angle(positions[:, 0])
        oSphericals[i][1] = positions[:, 1].mean()
      meanPositions = pv_spherical_domain.spherical_to_cartesian(oSphericals, r)
    else:
     meanPositions = np.empty((numClustersInThisBlock, 3))
     for i in range(numClustersInThisBlock):
        positions = clusteredPositions[cluIds == minClu + i]
        meanPositions[i] = positions.mean(axis=0)
    if self._onSphere:
      r = pv_spherical_domain.get_radius(iDataSet)
      comp_norm = np.linalg.norm
      for i, pos in enumerate(meanPositions):
        meanPositions[i] *= r/comp_norm(pos)
    oMeanSet.Points = meanPositions
    oMeanData.append(np.arange(minClu, endClu), cluAttrName)
    
    # Also add the mean of the point data
    for name in oPointData.keys():
      if name.startswith("cluster"):
        continue
      arr = oPointData[name]
      # works for scalars, vectors, matrices, ...
      means = np.empty((numClustersInThisBlock, *arr.shape[1:]), arr.dtype)
      for i in range(numClustersInThisBlock):
        means[i] = arr[cluIds == minClu + i].mean(axis=0)
      oMeanData.append(means, name)
    
    return 1
  
  #------------------------------------------------------------------------------------------------#
  
  def RequestData(self, request, iInfoVecTuple, oInfoVec):
    if request != self._lastRequest: # only once per multiblock
      self._numClusters = 0
      self._lastRequest = request
      self._blockCounter = 0
      name = type(self).__name__
      try:
        pxm = servermanager.ProxyManager()
        for proxy in pxm.GetProxiesInGroup('sources').values():
          if proxy.__class__.__name__ == self.__class__.__name__:
            if proxy.SMProxy.GetClientSideObject().GetInformation() == self.GetInformation():
              name = pxm.GetProxyName('sources', proxy)
      except AttributeError:
        pass # in the beginning there may be no ActiveConnection
      time = iInfoVecTuple[0].GetInformationObject(0).Get(self.GetExecutive().UPDATE_TIME_STEP())
      print('%s time %.2f  minPts/eps %d/%.2f  scaWgt %.1f' % (
        name, time, self._minPts, self._eps, self._scalarWeightInPercent))
    #print("--> RequestData %s block %d" % (name, self._blockCounter))
    
    iDataSet = vtkPointSet.GetData(iInfoVecTuple[0])
    MyClass = type(iDataSet)
    
    oCluSet = MyClass()
    oCluSet.SetFieldData(iDataSet.GetFieldData())
    oInfoVec.GetInformationObject(0).Set(vtkDataObject.DATA_OBJECT(), oCluSet)
    oMeanSet = MyClass()
    oMeanSet.SetFieldData(iDataSet.GetFieldData())
    oInfoVec.GetInformationObject(1).Set(vtkDataObject.DATA_OBJECT(), oMeanSet)
    oNoiseSet = MyClass()
    oNoiseSet.SetFieldData(iDataSet.GetFieldData())
    oInfoVec.GetInformationObject(2).Set(vtkDataObject.DATA_OBJECT(), oNoiseSet)
    
    wrap = lambda x: dsa.WrapDataObject(x)
    retCode = self.execute(wrap(iDataSet), wrap(oCluSet), wrap(oMeanSet), wrap(oNoiseSet))
    self._blockCounter += 1
    #print('--> %d clusters'%oMeanSet.GetNumberOfPoints())
    return retCode
