from math import sqrt, cos, sin, pi as PI, asin, atan2

import numpy as np
from numpy.linalg import norm as np_linalg_norm

from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import make_pipeline

import vtkmodules
from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.util import numpy_support
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import vtkCellArray, vtkPolyData
#from vtkmodules.vtkCommonExecutionModel import vtkStreamingDemandDrivenPipeline

from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain

from aaaown import pv_spherical_domain


RAD2DEG = 180. / PI
TWOPI = PI * 2.

#--------------------------------------------------------------------------------------------------#

def get_polylines(polyData):
  """ Get the polylines as a (ragged) numpy array of numpy arrays of point indices """
  
  cellArray = polyData.GetLines()
  numLines = cellArray.GetNumberOfCells()
  lines = np.empty(numLines, object)
  
  offsets = numpy_support.vtk_to_numpy(cellArray.GetOffsetsArray())
  cvity = numpy_support.vtk_to_numpy(cellArray.GetConnectivityArray())
  
  assert len(offsets) == numLines + 1 # conveniently there is a final pseudo offset
  for i in range(numLines):
    lines[i] = cvity[offsets[i] : offsets[i+1]]
  return lines

#--------------------------------------------------------------------------------------------------#

def prepare_longitudes(longitudes, polylines):
  """ Prepare angles so that they can be treated like any other scalars.
  
  We expect that the sequence of angles does not cover more than 180 degrees.
  This is a reasonable constraint for many applications (a greater range occurs rarely).
  It may happen, however, that the naive peak-to-peak distance is too large because it refers to
  the longer arc connecting the two extreme angels. This function remedies this.
  """

  for timeIdx2globalIdx in polylines:
    timeIdx2angleVal = longitudes[timeIdx2globalIdx] # copy
    peakToPeak = np.ptp(timeIdx2angleVal)
    if peakToPeak <= PI:
      continue
    #print("Adapting a polyline")
    # If the constraint mentioned above holds, there must be a 180 degrees gap between one pair of
    # consecutive angles when they are sorted by value (not by their position in the polyline).
    foundGap = False
    num = len(timeIdx2globalIdx)
    # We need to work indirectly because we don't want to rearrange the numbers in `longitudes`.
    # `timeIdx` is the local index when the angles are sorted by time (as with the input polyline);
    # `angleIdx` is the local index when the angles are sorted by value.
    angleIdx2timeIdx = np.argsort(timeIdx2angleVal)
    # We look at consecutive angle "objects" `a` and `b`
    a_angleVal = timeIdx2angleVal[angleIdx2timeIdx[0]]
    # (The previous peakToPeak value must be between the first and the last angle (sorted by value)
    # -- the gap between the "wrap-around" pair.)
    for b_angleIdx in range(1, num):
      b_angleVal = timeIdx2angleVal[angleIdx2timeIdx[b_angleIdx]]
      gap = b_angleVal - a_angleVal
      if gap < PI:
        a_angleVal = b_angleVal
        continue
      foundGap = True
      # Add two pi to all angles before the gap.
      # NOTE optimization possibility:
      # check which subset is larger and possibly subtract two pi from the second subsequence
      for angleIdx in range(b_angleIdx):
        longitudes[timeIdx2globalIdx[angleIdx2timeIdx[angleIdx]]] += TWOPI
      ## BEGIN testing
      #peakToPeak = np.ptp(longitudes[timeIdx2globalIdx])
      #if peakToPeak > PI:
        #print("Error in my logic: still %.1f degrees" % (peakToPeak*RAD2DEG))
      ## END testing
      break
    if not foundGap:
      print("Error: could not adapt the polyline:", timeIdx2angleVal)
  
  return longitudes

#--------------------------------------------------------------------------------------------------#

@smproxy.filter(label="Regression For Polyline")
@smproperty.input(name="Polylines")
@smdomain.datatype(dataTypes=["vtkPolyData"], composite_data_supported=True)
class RegressionForPolyline(VTKPythonAlgorithmBase):
  """ Apply linear regression to smooth the polylines of a dataset """
  
  def __init__ (self):
    VTKPythonAlgorithmBase.__init__(self, nInputPorts=1, nOutputPorts=1, outputType="vtkPolyData")
    self._attrNameParametrization = ""
    self._onSphere = False
    self._attrNameExtra = ""
    self._polyDegree = 3
    self._resampleWithGlobalTime = False
  
  #------------------------------------------------------------------------------------------------#
  
  
  @smproperty.intvector(name="ResampleAllAtAll", default_values=0)
  @smdomain.xml("""<BooleanDomain name="bool"/>""")
  def SetResampleAllAtAll(self, val):
    """Resample all lines at all found time steps (for easy comparability)?"""
    self._resampleAllAtAll = False if val == 0 else True
    self.Modified()
  
  @smproperty.xml("""
<StringVectorProperty name="Parametrization" number_of_elements="1" command="SetParametrization">
  <ArrayListDomain name="array_list" default_values="3">
    <RequiredProperties><Property function="Input" name="Polylines"/></RequiredProperties>
  </ArrayListDomain>
  <Documentation>Select the attribute that defines the parametrization of the polylines.</Documentation>
</StringVectorProperty>""")
  def SetParametrization(self, val):
    self._attrNameParametrization = val
    self.Modified()
  
  
  @smproperty.intvector(name="OnSphere", default_values=0)
  @smdomain.xml("""<BooleanDomain name="bool"/>""")
  def SetOnSphere(self, val):
    """ Do the input (Cartesian!) points all lie on a sphere?
    
    And should the output points therefore also all lie on a sphere with the same radius?
    """
    self._onSphere = False if val == 0 else True
    self.Modified()
  
  
  @smproperty.xml("""
<StringVectorProperty name="ExtraAttribute" number_of_elements="1" command="SetExtraAttribute">
  <ArrayListDomain name="array_list" default_values="3">
    <RequiredProperties><Property function="Input" name="Polylines"/></RequiredProperties>
  </ArrayListDomain>
  <Documentation>Select an attribute that is smoothed together with the spatial position of the samples.</Documentation>
</StringVectorProperty>""")
  def SetExtraAttribute(self, val):
    self._attrNameExtra = val
    self.Modified()
  
  
  @smproperty.intvector(name="Degree", default_values=3)
  @smdomain.intrange(min=1, max=25) # large degrees (> ~20) cannot be handled by doubles
  def SetDegree(self, val):
    """ Degree of the polynomial to fit the polyline """
    self._polyDegree = val
    self.Modified()
    
  #------------------------------------------------------------------------------------------------#
  
  def execute(self, iPolyData, oPolyData):
    iPointData = iPolyData.PointData
    # We treat a polyline as a 1D to kD function (k either 2, 3 or 4)
    iDomain = iPointData[self._attrNameParametrization]
    if type(iDomain) == dsa.VTKNoneArray:
      return 0
    iImage = iPolyData.Points # 3D
    iExtra = iPointData[self._attrNameExtra]
    haveExtra = type(iExtra) != dsa.VTKNoneArray
    polylines = get_polylines(iPolyData)
    
    if self._onSphere:
      iImage = pv_spherical_domain.cartesian_to_spherical(iImage) # 2D
    if haveExtra:
      iImage = np.c_[iImage, iExtra] # earlier + 1
    
    if self._onSphere:
      # The selection of the first column returns a view, so we do not need a return value from 
      # prepare_longitudes. It is okay to edit the data in-place because the longitudes have been 
      # computed in their own array in this filter anyway.
      prepare_longitudes(iImage[:, 0], polylines)
    
    polyDegree = self._polyDegree
    minNumSamples = len(polylines[0])
    for polyline in polylines:
      num = len(polyline)
      if num < minNumSamples:
        minNumSamples = num
    maxPossibleDeg = minNumSamples - 1
    if polyDegree > maxPossibleDeg:
      print("The selected degree (%d) is too large. Because of at least one polyline with only few samples it is limited to %d" % (polyDegree, maxPossibleDeg))
      polyDegree = maxPossibleDeg
    
    resampleAllAtAll = self._resampleAllAtAll
    #sharedDom = self.GetInputInformation(0, 0).Get(vtkStreamingDemandDrivenPipeline.TIME_STEPS()) if resampleAllAtAll else None
    #print("sharedDom:")
    #print(sharedDom)
    sharedDom = np.unique(iDomain) if resampleAllAtAll else None
    numT = len(sharedDom) if sharedDom else None
    
    domToConcat = []
    imgToConcat = []
    polylinesNew = []
    nxtPtIdx = 0
    model = make_pipeline(PolynomialFeatures(polyDegree), LinearRegression())
    for polyline in polylines:
      iNum = len(polyline)
      iDom = iDomain[polyline]
      iImg = iImage[polyline]
      oDom, oNum = (sharedDom, numT) if resampleAllAtAll else (iDom, iNum)
      
      # Center the domain at 0 for more numerical stability at the two sides (not only the side
      # with higher absolute values).
      # Scale the domain to an expected sampling distance of 1 in case the domain range happens to
      # be very large.
      # (This transformation does not make any difference in the result theoretically;
      # i.a. for correct color-mapping, however, we must take care to only do this internally).
      offset = (iDom[0] + iDom[-1]) / -2
      factor = (iNum-1) / (iDom[-1]-iDom[0]) # target expected distance between two samples is 1
      # our domain is 1D but we need rows
      model.fit(((iDom+offset)*factor).reshape(iNum, 1), iImg)
      
      oImg = model.predict(((oDom+offset)*factor).reshape(oNum, 1))
      domToConcat.append(oDom)
      imgToConcat.append(oImg)
      polylinesNew.append(np.arange(nxtPtIdx, nxtPtIdx+oNum))
      nxtPtIdx += oNum
    oDomain = np.concatenate(domToConcat)
    oImage = np.concatenate(imgToConcat)
    polylines = polylinesNew
    
    oPoints = oImage
    oExtra = None
    if haveExtra:
      oPoints = oImage[:, 0:-1]
      oExtra = oImage[:, -1]
    if self._onSphere:
      oPoints = pv_spherical_domain.spherical_to_cartesian(
        oPoints, pv_spherical_domain.get_radius(iPolyData))
    
    vtkCells = vtkCellArray()
    for polyline in polylines:
      vtkCells.InsertNextCell(len(polyline), polyline)
    oPolyData.SetLines(vtkCells)
    
    oPolyData.Points = oPoints
    oPointData = oPolyData.PointData
    oPointData.append(np.array(oDomain), self._attrNameParametrization)
    if haveExtra:
      oPointData.append(np.array(oExtra), self._attrNameExtra)
    
    if haveExtra and self._onSphere:
      self.comp_cell_data(oPolyData, oPoints, oExtra, polylines)
    #print("<-- END", type(self).__name__)
    return 1
  
  #------------------------------------------------------------------------------------------------#
  
  def comp_cell_data(self, oPolyData, cartesians, scalars, polylines):
    '''This is a quick automation of what was done manually (with the GUI) before'''
    n = len(polylines)
    earthDist = np.empty((n), 'f8')
    scalarsFst = np.empty((n), 'f8') # hard-coded data type
    scalarsLst = np.empty((n), 'f8')
    angle_rad = pv_spherical_domain.angle_between_two_points
    for i, polyline in enumerate(polylines):
      poiFst = polyline[0]
      poiLst = polyline[-1]
      earthDist[i] = angle_rad(cartesians[poiFst], cartesians[poiLst]) * 6371.0088
      scalarsFst[i] = scalars[poiFst]
      scalarsLst[i] = scalars[poiLst]
    cData = oPolyData.CellData
    cData.append(earthDist, 'arcOnEarthInKm')
    cData.append(scalarsFst, self._attrNameExtra+'First')
    cData.append(scalarsLst, self._attrNameExtra+'Last')
    cData.append(np.arange(n), 'explicitIdx')
    return self
  
  #------------------------------------------------------------------------------------------------#
  
  def RequestData(self, request, iInfoVecTuple, oInfoVec):
    iPolyData = vtkPolyData.GetData(iInfoVecTuple[0])
    oPolyData = vtkPolyData.GetData(oInfoVec)
    #print("--> BEGIN", type(self).__name__)
    return self.execute(dsa.WrapDataObject(iPolyData), dsa.WrapDataObject(oPolyData))
