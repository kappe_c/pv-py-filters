from math import sqrt, pi as PI, cos, acos, exp

import numpy as np

from vtk import vtkStaticPointLocator, vtkIdList
import vtkmodules
from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import vtkUnstructuredGrid

from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain

from aaaown import pv_spherical_domain


DEG2RAD = PI / 180.


@smproxy.filter(label="Kernel Density Estimation")
@smproperty.input(name="Domain", port_index=1)
@smdomain.datatype(dataTypes=["vtkUnstructuredGrid"], composite_data_supported=True)
@smproperty.input(name="Points", port_index=0)
@smdomain.datatype(dataTypes=["vtkUnstructuredGrid"], composite_data_supported=True)
class KernelDensityEstimation(VTKPythonAlgorithmBase):
  """ Kernel Density Estimation.
  
  TODO Doc
  
  NOTE Currently only implemented for points on a sphere (given in Cartesian x-, y-, z-coordinates)
  """
  
  def __init__ (self):
    VTKPythonAlgorithmBase.__init__(
      self, nInputPorts=2, nOutputPorts=1, outputType="vtkUnstructuredGrid")
    self._onSphere = False # just a stub for now
    self._bandwidth = 5.
    self._divisor = 0
    #self._minAllowedDensity = 0.
  
  #------------------------------------------------------------------------------------------------#
  
  
  @smproperty.doublevector(name="Bandwidth", default_values=5.)
  @smdomain.doublerange(min=0., max=180.)
  def SetBandwidth(self, val):
    """ Radius of the ball centered at a sample position, in which points have to lie
    to be considered in the estimation.
    NOTE This value is currently interpreted as degrees (think of the arclength between two points).
    """
    self._bandwidth = val
    self.Modified()
  
  
  @smproperty.intvector(name="Divisor", default_values=0)
  @smdomain.intrange(min=0)
  def SetDivisor(self, val):
    """ The density values are divided by this integer.
    (The intent is to be able to interpret the values as probabilities directly.)
    If this parameter is set to 0, the value of the field data *original number of blocks* will be
    used, or 1 if there is no such data.
    """
    self._divisor = val
    self.Modified()
  
  
  #@smproperty.doublevector(name="MinimumAllowedDensity", default_values=0.)
  #@smdomain.doublerange(min=0., max=100.)
  #def SetMinimumAllowedDensity(self, val):
    #""" If the density at one output vertex is less than this value, it gets pruned
    #(and the cells it is part of); 0 means no pruning of the mesh.
    #Only one point found, but exactly at the sample position leads to density 1;
    #other typical values depend on the data distribution, the kernel and the bandwidth.
    #"""
    #self._minAllowedDensity = val
    #self.Modified()
  
  #------------------------------------------------------------------------------------------------#
  
  @staticmethod
  def get_divisor(dataset):
    res = None
    if type(dataset.FieldData.GetArray("original number of blocks")) != dsa.VTKNoneArray:
      res = dataset.FieldData["original number of blocks"]
      print("Divisor (by *original number of blocks*): %d" % res)
    else:
      res = len(dataset.Points)
      #print("Divisor (by number of points): %d" % res)
    return res
  
  #------------------------------------------------------------------------------------------------#
  
  def execute(self, iPts, iFld, oDataset):
    bandwidthInRad = self._bandwidth * DEG2RAD
    ptsRadius = pv_spherical_domain.get_radius(iPts)
    fldRadius = pv_spherical_domain.get_radius(iFld)
    if not np.isclose(ptsRadius, fldRadius):
      print("Error: *Points* radius (%f) does not equal *Domain* radius (%f)" % (ptsRadius, fldRadius))
      return 0
    # The ParaView search structures only use the Euclidean distance for a ball search.
    # Luckily, for points on a sphere (common `fldRadius`), the Euclidean distance is equivalent to
    # the angle. The following formula holds: d_eucl = r * sqrt(2 * (1 - cos))
    searchRadius = fldRadius * sqrt(2. * (1. - cos(bandwidthInRad)))
    
    # compute diffInRad for the kernel, in [0, pi]
    # Notice that while the Euclidean distance is equivalent to the arclength for a ball search,
    # it would make a difference (in the final weight) when used as input to the kernel.
    # Here we use the angle in radians; this can be used because the arclength depends simply
    # linearly on it: arclength = angleInRad * fldRadius
    normalizer = 1. / (fldRadius * fldRadius)
    #d_func = lambda p, q : 0. if np.array_equal(p, q) else acos(np.dot(p, q) * normalizer)
    def d_func(p, q): # dealing with numerical instability
      if np.array_equal(p, q):
        return 0.
      tmpCos = np.dot(p, q) * normalizer
      if tmpCos <= -1.:
        return PI
      if tmpCos >= 1.:
        return 0.
      return acos(tmpCos)
    
    k_func = None
    #kernelName = "" # fallback: linear
    kernelName = "gaussian"
    #kernelName = "cos_full"
    # The maximum diffInRad input to the kernel is bandwidthInRad (greater leads to weight 0).
    if kernelName == "cos_full":
      scaler = PI / bandwidthInRad # precompute this just for performance
      # diff=0 … cos(0)=1 … w=1, diff=bandwidthInRad … cos(pi)=-1 … w = 0
      k_func = lambda diffInRad: (cos(diffInRad * scaler) + 1.) / 2.
    elif kernelName == "gaussian":
      s = bandwidthInRad / 3. # cutting after 99.73 % of the whole
      k_func = lambda diffInRad: exp(-0.5 * (diffInRad/s)**2)
    else: # linear
      k_func = lambda diffInRad: 1. - abs(diffInRad)/bandwidthInRad
    
    iPoints = iPts.Points
    #print("Number of input points: %d" % len(iPoints))
    samplePositions = iFld.Points
    #print("Number of sample positions: %d" % len(samplePositions))
    
    densityArr = np.empty(len(samplePositions), "f8")
    divisor = self._divisor if self._divisor != 0 else self.get_divisor(iPts)
    #print("Divisor: %d" % divisor)
    density2probability = 1. / divisor
    
    # In the first pass, iterate over the whole mesh of the input field,
    # processing the input points with the kernel.
    
    iPtsLocator = vtkStaticPointLocator()
    iPtsLocator.SetDataSet(iPts.VTKObject)
    ptIdxes = vtkIdList() # output parameter (reuse for performance)
    for i, pos in enumerate(samplePositions):
      iPtsLocator.FindPointsWithinRadius(searchRadius, pos, ptIdxes)
      # vtkIdList is not iterable :-(
      numPtsRelevant = ptIdxes.GetNumberOfIds()
      if numPtsRelevant == 0:
        densityArr[i] = 0.
      elif numPtsRelevant == 1:
        idx = ptIdxes.GetId(0)
        densityArr[i] = k_func(d_func(pos, iPoints[idx])) * density2probability
      else:
        #print("i: %d => numPtsRelevant: %d" % (i, numPtsRelevant))
        idxList = [ptIdxes.GetId(j) for j in range(numPtsRelevant)]
        weightArr = np.fromiter((k_func(d_func(pos, iPoints[idx])) for idx in idxList), "f8")
        densityArr[i] = weightArr.sum() * density2probability
    
    
    # In the optional second pass, iterate over the intermediate output mesh,
    # filtering out vertices (and triangles).
    #minDensity = self._minAllowedDensity
    #if minDensity > 0.:
      #print("Warning: pruning not implemented yet") # TODO impl
      #for i, density in enumerate(densityArr):
        #if density < minDensity:
          #pass
    
    
    # Setup the output dataset
    
    oDataset.Points = samplePositions
    oPointData = oDataset.PointData
    oPointData.append(densityArr, "density")
    
    oDataset.SetCells(iFld.CellTypes, iFld.CellLocations, iFld.Cells)
    oCellData = oDataset.CellData
    iCellData = iFld.CellData
    for name in iCellData.keys(): # there is no .items() :-(
      oCellData.append(iCellData[name], name)
    
    return 1
  
  #------------------------------------------------------------------------------------------------#
  
  def RequestData(self, request, iInfoVecTuple, oInfoVec):
    iPts = dsa.WrapDataObject(vtkUnstructuredGrid.GetData(iInfoVecTuple[0]))
    iFld = dsa.WrapDataObject(vtkUnstructuredGrid.GetData(iInfoVecTuple[1]))
    oDataset = dsa.WrapDataObject(vtkUnstructuredGrid.GetData(oInfoVec))
    return self.execute(iPts, iFld, oDataset)
