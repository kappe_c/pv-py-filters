from math import sqrt

import numpy as np

from scipy import interpolate
from scipy.optimize import linear_sum_assignment

from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.util import numpy_support
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import vtkCellArray, vtkPolyData

from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain

from aaaown import pv_spherical_domain

#--------------------------------------------------------------------------------------------------#

def get_polylines(polyData):
  """Get the polylines as a (ragged) numpy array of numpy arrays of point indices"""
  # code clone from RegressionForPolyline
  cellArray = polyData.GetLines()
  numLines = cellArray.GetNumberOfCells()
  lines = np.empty(numLines, object)
  
  offsets = numpy_support.vtk_to_numpy(cellArray.GetOffsetsArray())
  cvity = numpy_support.vtk_to_numpy(cellArray.GetConnectivityArray())
  
  assert len(offsets) == numLines + 1 # conveniently there is a final pseudo offset
  for i in range(numLines):
    lines[i] = cvity[offsets[i] : offsets[i+1]]
  return lines

#--------------------------------------------------------------------------------------------------#

def componentwise_dist_angular(a, b):
  """Assuming a and b have the same length n,
  returns the component-wise angular distance in radians in an f8 array of length n.
  """
  comp_ang = pv_spherical_domain.angle_between_two_points
  o = np.empty(len(a), "f8")
  for i, (ai, bi) in enumerate(zip(a, b)):
    o[i] = comp_ang(ai, bi)
  return o

#--------------------------------------------------------------------------------------------------#

def root_mean_square(a):
  return sqrt((a**2).mean())

#--------------------------------------------------------------------------------------------------#

def dists_between_lines_and_points(l2s2timeA, l2s2pointA, l2s2timeB, l2s2pointB):
  """This not only computes the distances but also matches the lines based on this."""
  numA = len(l2s2pointA)
  numB = len(l2s2pointB)
  l2l2dist = np.full((numA, numB), 7., "f8")
  l2l2s2dist = np.full((numA, numB), None, object)
  for i in range(numA):
    s2timeA = l2s2timeA[i]
    s2pointA = l2s2pointA[i]
    j2dist = l2l2dist[i]
    j2s2dist = l2l2s2dist[i]
    #print("s2timeA:", s2timeA.shape, s2timeA.dtype)
    #print(s2timeA)
    for j in range(numB):
      f_B = interpolate.interp1d(l2s2timeB[j], l2s2pointB[j], axis=0, copy=False,
                                 fill_value="extrapolate", assume_sorted=True)
      # This does not do slerp but it is okay because we compute
      # the angular distance between the points.
      pointsB = f_B(s2timeA)
      dVec = componentwise_dist_angular(s2pointA, pointsB)
      dSca = root_mean_square(dVec)
      #print("dist %d,%d:" % (i, j), dSca)
      j2dist[j] = dSca
      l2l2dist[i, j] = dSca
      j2s2dist[j] = dVec
      l2l2s2dist[i, j] = dVec
  rowIndices, colIndices = linear_sum_assignment(l2l2dist)
  print("Matching:", ", ".join(map(lambda ij: "%d->%d"%ij, zip(rowIndices, colIndices))))
  
  l2dist = l2l2dist[rowIndices, colIndices]
  #print("Line-to-line distances, RMS in degrees")
  #print(l2dist*(180./np.pi))
  
  # Currently we expect that each line from the first input is matched, and the returned indices
  # are an ordered gapless sequence starting at 0.
  assert np.array_equal(rowIndices, np.arange(numA))
  
  return rowIndices, colIndices, l2dist, l2l2s2dist[rowIndices, colIndices]

#--------------------------------------------------------------------------------------------------#

@smproxy.filter(label="Polyline Comparison")
@smproperty.input(name="ToEvaluate", port_index=1)
@smdomain.datatype(dataTypes=["vtkPolyData"], composite_data_supported=True)
@smproperty.input(name="GroundTruth", port_index=0)
@smdomain.datatype(dataTypes=["vtkPolyData"], composite_data_supported=True)
class PolylineComparison(VTKPythonAlgorithmBase):
  """Compare two sets of polylines
  NOTE This is a relatively quick hack. Among other things, it assumes the polylines are all on
  the same sphere, measuring the distance between a pair of sample points as the angle between them
  and the overall distance as the root of the mean of the squared distances.
  The output of the filter is the first ("GroundTruth") dataset with the distance values added to
  each point and polyline.
  """
  
  def __init__ (self):
    VTKPythonAlgorithmBase.__init__(self, nInputPorts=2, nOutputPorts=1, outputType="vtkPolyData")
    self._attrNameParametrization = ""
    self._onSphere = False
  
  #------------------------------------------------------------------------------------------------#
  
  @smproperty.xml("""
<StringVectorProperty name="Parametrization" number_of_elements="1" command="SetParametrization">
  <ArrayListDomain name="array_list" default_values="-1">
    <RequiredProperties><Property function="Input" name="GroundTruth"/></RequiredProperties>
  </ArrayListDomain>
  <Documentation>Select the attribute that defines the parametrization of the polylines.</Documentation>
</StringVectorProperty>""")
  def SetParametrization(self, val):
    self._attrNameParametrization = val
    self.Modified()
  
  
  @smproperty.intvector(name="OnSphere", default_values=0)
  @smdomain.xml("""<BooleanDomain name="bool"/>""")
  def SetOnSphere(self, val):
    """Do the input (Cartesian!) points all lie on the same sphere?"""
    self._onSphere = False if val == 0 else True
    self.Modified()
    
  #------------------------------------------------------------------------------------------------#
  
  def execute(self, polydataA, polydataB, polydataO):
    # In the following, l refers to a polyline index within a dataset, s refers to a sample index
    # within a polyline and p refers to a point index within a dataset.
    # The suffixes A an B refer to the first and second input dataset.
    
    if not self._onSphere:
      print("Error: currently only polylines on a common sphere are supported. Going on pretending this is the case")
    
    l2s2pA = get_polylines(polydataA)
    l2s2pB = get_polylines(polydataB)
    print("Comparing %d against %d polylines" % (len(l2s2pA), len(l2s2pB)))
    #print("l2s2pA:")
    #print(l2s2pA)
    
    ragged_arr = lambda fn, it: np.array(tuple(map(fn, it)), object)
    
    l2s2pointA = ragged_arr(lambda s2p: polydataA.Points[s2p], l2s2pA)
    l2s2pointB = ragged_arr(lambda s2p: polydataB.Points[s2p], l2s2pB)
    
    # time means parametrization value here regardless of the actual attribute name
    attrName = self._attrNameParametrization
    p2timeA = polydataA.PointData[attrName]
    p2timeB = polydataB.PointData[attrName]
    l2s2timeA = None
    l2s2timeB = None
    
    if type(p2timeA) == dsa.VTKNoneArray:
      print('Warning: the polydata dataset used for output does not have the point data "%s"; using an integer sequence instead' % attrName)
      l2s2timeA = [np.arange(len(s2p)) for s2p in l2s2pA]
    else:
      l2s2timeA = [p2timeA[s2p] for s2p in l2s2pA]
    
    if type(p2timeB) == dsa.VTKNoneArray:
      print('Warning: the polydata dataset used for reference does not have the point data "%s"; using an integer sequence instead' % attrName)
      l2s2timeB = [np.arange(len(s2p)) for s2p in l2s2pB]
    else:
      l2s2timeB = [p2timeB[s2p] for s2p in l2s2pB]
    
    rowIndices, colIndices, l2dist, l2s2dist = dists_between_lines_and_points(
      l2s2timeA, l2s2pointA, l2s2timeB, l2s2pointB)
    
    # arange the point distances to fit the input data order
    p2dist = np.empty(len(polydataA.Points), "f8")
    for l, s2dist in enumerate(l2s2dist):
      for s, dist in enumerate(s2dist):
        p2dist[ l2s2pA[l][s] ] = dist
    
    
    # make the output (cell data and point data)
    
    #vtkCells = vtkCellArray()
    #for indices in l2s2pB:
      #vtkCells.InsertNextCell(len(indices), indices)
    #polydataO.SetLines(vtkCells)
    polydataO.CellData.append(l2dist*(180./np.pi), 'rmsInDeg')
    
    #polydataO.Points = polydataA.Points
    # the angles the way they contribute to rms
    #polydataO.PointData.append(p2dist**2, "distInRadSquared")
    # the angles in a more intuitive way, good for debugging
    polydataO.PointData.append(p2dist*(180./np.pi), "distInDeg")
    
    return 1
  
  #------------------------------------------------------------------------------------------------#
  
  def RequestData(self, request, iInfoVecTuple, oInfoVec):
    polydataA = vtkPolyData.GetData(iInfoVecTuple[0])
    polydataB = vtkPolyData.GetData(iInfoVecTuple[1])
    polydataO = vtkPolyData.GetData(oInfoVec)
    polydataO.ShallowCopy(polydataA)
    return self.execute(*map(dsa.WrapDataObject, (polydataA, polydataB, polydataO)))
