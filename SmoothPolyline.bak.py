from math import sqrt, cos, sin, pi as PI, asin, atan2

import numpy as np
from numpy.linalg import norm as np_linalg_norm

import vtkmodules
from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.util import numpy_support
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import vtkCellArray, vtkPolyData

from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain

from aaaown import pv_spherical_domain
from aaaown.average_angle import average_angle, average_angle_of_two
from aaaown.map1dKd_smoothing import smooth_curves

RAD2DEG = 180. / PI
TWOPI = PI * 2.
EARTH_RADIUS = 6371 # km, approx

#--------------------------------------------------------------------------------------------------#

def get_polylines(polyData):
  """ Get the polylines as a (ragged) numpy array of numpy arrays of point indices """
  
  cellArray = polyData.GetLines()
  numLines = cellArray.GetNumberOfCells()
  lines = np.empty(numLines, object)
  
  offsets = numpy_support.vtk_to_numpy(cellArray.GetOffsetsArray())
  cvity = numpy_support.vtk_to_numpy(cellArray.GetConnectivityArray())
  
  assert len(offsets) == numLines + 1 # conveniently there is a final pseudo offset
  for i in range(numLines):
    lines[i] = cvity[offsets[i] : offsets[i+1]]
  return lines

#--------------------------------------------------------------------------------------------------#

def fill_gaps(dom, img, polylines, mean_func=None):
  """ The `mean_func` takes two image points and a corresponding sequence of means. """
  
  # Get the regular sampling scale as the minimum distance between any subsequent samples
  s = dom[polylines[0][1]] - dom[polylines[0][0]]
  for polyline in polylines:
    dom_p = dom[polyline[0]]
    for i in range(1, len(polyline)):
      dom_q = dom[polyline[i]]
      s_p = dom_q - dom_p
      if s_p < s:
        s = s_p
      dom_p = dom_q
  #print("Found sampling scale of parametrization space to be %.3f" % s)
  assert s > 0 # dividing by s later
  
  numOld = len(dom)
  assert(len(img) == numOld)
  nxtPtIdx = numOld
  domExtra = []
  imgExtra = []
  
  if mean_func is None:
    mean_func = lambda p, q, ws: p * ws[0] + q * ws[1]
  
  for polyIdx, iPolyline in enumerate(polylines):
    # you must not insert into containers you are iterating on
    insertBeforeThese = [] # before this original index a gap occurs
    insertionNumbers = [] # this many points must be inserted
    oPolyline = None
    
    dom_p = dom[iPolyline[0]]
    img_p = img[iPolyline[0]]
    for i in range(1, len(iPolyline)):
      q = iPolyline[i]
      dom_q = dom[q]
      img_q = img[q]
      
      s_pq = dom_q - dom_p
      #assert s_pq > 0
      
      m = int(round(s_pq / s)) # target number of segments between p and q
      # numpy round overwrites the builtin and does not return an int
      
      if m > 1:
        insertBeforeThese.append(i)
        insertionNumbers.append(m - 1)
        for j in range(1, m): # note: could handle case m=2 specially for performance
          w_q = j / m
          w_p = 1. - w_q
          domExtra.append(dom_p * w_p + dom_q * w_q)
          imgExtra.append(mean_func(img_p, img_q, (w_p, w_q)))
      
      dom_p = dom_q
      img_p = img_q
    
    if not insertBeforeThese:
      continue
    
    oPolyline = []
    copyFromHere = 0
    for insertBeforeThis, numIns in zip(insertBeforeThese, insertionNumbers):
      oPolyline.extend(iPolyline[copyFromHere : insertBeforeThis])
      copyFromHere = insertBeforeThis
      for _ in range(numIns):
        oPolyline.append(nxtPtIdx)
        nxtPtIdx += 1
    oPolyline.extend(iPolyline[copyFromHere:])
    polylines[polyIdx] = oPolyline
  
  if domExtra:
    numNew = len(domExtra)
    #print("Adding %d points (%.1f %%) to the %d original ones to fill gaps" % \
      #(numNew, numNew / numOld, numOld))
    dom = np.append(dom, domExtra, axis=0)
    img = np.append(img, imgExtra, axis=0)
  return dom, img

#--------------------------------------------------------------------------------------------------#

@smproxy.filter(label="Smooth Polyline")
@smproperty.input(name="Polylines")
@smdomain.datatype(dataTypes=["vtkPolyData"], composite_data_supported=True)
class SmoothPolyline(VTKPythonAlgorithmBase):
  """ Smooth the polylines of a dataset
  
  We assume the parametrization domain is generally sampled regularly but the polylines may be
  missing some samples; these gaps are filled by linear interpolation before the smoothing so that
  with the moving average approach there is always the same weight in the past and the future of
  an output sample.
  """
  
  def __init__ (self):
    VTKPythonAlgorithmBase.__init__(self, nInputPorts=1, nOutputPorts=1, outputType="vtkPolyData")
    self._attrNameParametrization = ""
    self._onSphere = False
    self._attrNameExtra = ""
    self._bandwidthInNumPts = 6
    self._cropWidthInNumPts = 0
  
  #------------------------------------------------------------------------------------------------#
  
  @smproperty.xml("""
<StringVectorProperty name="Parametrization" number_of_elements="1" command="SetParametrization">
  <ArrayListDomain name="array_list" default_values="3">
    <RequiredProperties><Property function="Input" name="Polylines"/></RequiredProperties>
  </ArrayListDomain>
  <Documentation>Select the attribute that defines the parametrization of the polylines.</Documentation>
</StringVectorProperty>""")
  def SetParametrization(self, val):
    self._attrNameParametrization = val
    self.Modified()
  
  
  @smproperty.intvector(name="OnSphere", default_values=0)
  @smdomain.xml("""<BooleanDomain name="bool"/>""")
  def SetOnSphere(self, val):
    """ Do the input (Cartesian!) points all lie on a sphere?
    
    And should the output points therefore also all lie on a sphere with the same radius?
    """
    self._onSphere = False if val == 0 else True
    self.Modified()
  
  
  @smproperty.xml("""
<StringVectorProperty name="ExtraAttribute" number_of_elements="1" command="SetExtraAttribute">
  <ArrayListDomain name="array_list" default_values="3">
    <RequiredProperties><Property function="Input" name="Polylines"/></RequiredProperties>
  </ArrayListDomain>
  <Documentation>Select an attribute that is smoothed together with the spatial position of the samples.</Documentation>
</StringVectorProperty>""")
  def SetExtraAttribute(self, val):
    self._attrNameExtra = val
    self.Modified()
  
  
  @smproperty.intvector(name="Bandwidth", default_values=6)
  @smdomain.intrange(min=1)
  def SetBandwidth(self, val):
    """  Bandwidth of the smoothing window
    
    where value k means that for every output sample k "past" and k "future" points are
    considered.
    (Half of the smoothing window reaches into the past and half into the future;
    the bandwidth (in parametrization space) is half of the window width.)
    """
    self._bandwidthInNumPts = val
    self.Modified()
  
  
  @smproperty.intvector(name="CropWidth", default_values=0)
  @smdomain.intrange(min=0)
  def SetCropWidth(self, val):
    """  Number of points to crop from each end
    
    Should be less than or equal *Bandwidth*. The reason this parameter exists is because the first
    and last *bandwidth* points are not smoothed as much as the inner points of a polyline;
    so the original points from the begin and end of a polyline are "over-represented" in
    the result, which is not justified in general.
    """
    self._cropWidthInNumPts = val
    self.Modified()
    
  #------------------------------------------------------------------------------------------------#
  
  def execute(self, iPolyData, oPolyData):
    if self._attrNameParametrization == "":
      return 0
    
    iPointData = iPolyData.PointData
    haveExtra = self._attrNameExtra != ""
    bw = self._bandwidthInNumPts
    polylines = get_polylines(iPolyData)
    
    # We treat a polyline as a 1D to kD function (k either 2, 3 or 4)
    dom = iPointData[self._attrNameParametrization]
    img = iPolyData.Points # 3D
    if self._onSphere:
      img = pv_spherical_domain.cartesian_to_spherical(img) # 2D
    if haveExtra:
      img = np.c_[img, iPointData[self._attrNameExtra]] # earlier + 1
    
    mean_of_two = None # here we know the two angles are less than 2pi apart, so no modulo needed
    mean_of_seq = None
    if self._onSphere:
      if not haveExtra:
        mean_of_two = lambda p, q, ws: \
          average_angle_of_two(p[0], q[0], ws), p[1]*ws[0] + q[1]*ws[1]
        mean_of_seq = lambda listOfLonLat, ws: \
          average_angle(listOfLonLat[:, 0] % TWOPI, ws), np.average(listOfLonLat[:, 1], 0, ws)
      else:
        mean_of_two = lambda p, q, ws: \
          (average_angle_of_two(p[0], q[0], ws), *(p[1:3]*ws[0] + q[1:3]*ws[1]))
        def mean_of_seq(listOfLonLatExtra, ws):
          listOfLon = listOfLonLatExtra[:, 0] % TWOPI
          listOfLatExtra = listOfLonLatExtra[:, 1:3]
          return (average_angle(listOfLon, ws), *np.average(listOfLatExtra, 0, ws))
    
    # may change the individual polylines but not the container created in get_polylines
    dom, img = fill_gaps(dom, img, polylines, mean_of_two)
    
    img = smooth_curves(img, polylines, bw, "cosful", mean_of_seq)
    if self._cropWidthInNumPts > 0:
      numToDel = self._cropWidthInNumPts
      minNum = numToDel * 2 + 2 # at least one line segment must remain
      domToConcat = []
      imgToConcat = []
      polylinesNew = []
      nxtPtIdx = 0
      for polyline in polylines:
        num = len(polyline)
        if num < minNum:
          continue
        domToConcat.append(dom[polyline][numToDel:-numToDel])
        imgToConcat.append(img[polyline][numToDel:-numToDel])
        num -= numToDel * 2
        polylinesNew.append(np.arange(nxtPtIdx, nxtPtIdx + num))
        nxtPtIdx += num
      dom = np.concatenate(domToConcat)
      img = np.concatenate(imgToConcat)
      polylines = polylinesNew
    
    oPoints = img
    oExtra = None
    if haveExtra:
      oPoints = img[:, 0:-1]
      oExtra = img[:, -1]
    if self._onSphere:
      oPoints = pv_spherical_domain.spherical_to_cartesian(
        oPoints, pv_spherical_domain.get_radius(iPolyData))
    
    vtkCells = vtkCellArray()
    for polyline in polylines:
      vtkCells.InsertNextCell(len(polyline), polyline)
    oPolyData.SetLines(vtkCells)
    
    # compute and save the distance between the first and last point
    if self._onSphere:
      dsInRad = np.empty((len(polylines)), "f4")
      for i, polyline in enumerate(polylines):
        p = oPoints[polyline[0]]
        q = oPoints[polyline[-1]]
        dsInRad[i] = pv_spherical_domain.angle_between_two_points(p, q)
      oPolyData.CellData.append(dsInRad, "distanceInRadians")
      # one could also use the calculator (filter) to derive these two, this is just for convenience
      oPolyData.CellData.append(dsInRad * RAD2DEG, "distanceInDegrees")
      oPolyData.CellData.append(dsInRad * EARTH_RADIUS, "distanceOnEarthInKm")
    #else:
      #print("Warning: ")
    
    oPolyData.Points = oPoints
    oPointData = oPolyData.PointData
    oPointData.append(np.array(dom), self._attrNameParametrization)
    if haveExtra:
      oPointData.append(np.array(oExtra), self._attrNameExtra)
    
    #print("<-- END", type(self).__name__)
    return 1
  
  #------------------------------------------------------------------------------------------------#
  
  def RequestData(self, request, iInfoVecTuple, oInfoVec):
    iPolyData = vtkPolyData.GetData(iInfoVecTuple[0])
    oPolyData = vtkPolyData.GetData(oInfoVec)
    if self._bandwidthInNumPts < 1:
      print("[%s] pass-through mode because bandwidth is %d" % \
        (type(self).__name__, self._bandwidthInNumPts))
      # The output used to be a shallow copy of the input by default;
      # this does not seem to be the case any more.
      oPolyData.ShallowCopy(iPolyData)
      return 1
    #print("--> BEGIN", type(self).__name__)
    return self.execute(dsa.WrapDataObject(iPolyData), dsa.WrapDataObject(oPolyData))
