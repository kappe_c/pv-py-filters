# based on http://lyoncalcul.univ-lyon1.fr/events/2019/PythonAlgorithmExamples.py on June 3, 2020

from functools import reduce
import os

import numpy as np
import pandas as pd
import xarray as xr # v >= 0.15

from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.util import numpy_support
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonCore import vtkDataArraySelection
from vtkmodules.vtkCommonDataModel import vtkMultiBlockDataSet, vtkRectilinearGrid

from paraview.util.vtkAlgorithm import smproxy, smproperty#, smdomain


NS_PER_DAY = 1e9 * 60 * 60 * 24


def createModifiedCallback(anobject):
  import weakref
  weakref_obj = weakref.ref(anobject)
  anobject = None
  def _markmodified(*args, **kwars):
    o = weakref_obj()
    if o is not None:
      o.Modified()
  return _markmodified



@smproxy.source(name="BlockPerNetcdf", label="Multiblock Dataset from netCDF Files")
class BlockPerNetcdf(VTKPythonAlgorithmBase):
  """Create a multiblock dataset from netCDF files (intended for ensemble data)"""
  def __init__(self):
    VTKPythonAlgorithmBase.__init__(
      self, nInputPorts=0, nOutputPorts=1, outputType="vtkMultiBlockDataSet")#"vtkRectilinearGrid")
    self._dirpath = ""
    self._filenamePattern = "prefix%02d.nc"
    self._filenameRange = (0, 0, 1)
    
    self._xds = None
    self._memberDimName = "member" # only used internally
    
    self._timeDimName = "time" # currently hard-coded
    # There are three types involved concerning time here.
    # Integers mapping to floats for the Paraview dropdown selection, and
    # numpy.datetime64 objects with ns precision provided by xarray.
    self._timefloats = None
    # Paraview only gives us the floats but we need either the objects or the integers for the
    # xarray data selection.
    self._timefloat2object = None
    
    self._xvalues = None
    self._yvalues = None
    self._zvalues = None
    
    self._arrayselection = vtkDataArraySelection()
    self._arrayselection.AddObserver("ModifiedEvent", createModifiedCallback(self))
  
  #------------------------------------------------------------------------------------------------#
  
  @smproperty.stringvector(name="Directory", default_values="")
  def SetDirectory(self, val):
    """ Absolute or relative path (from pwd) to the directory containing the netCDF files
    
    May begin with `~` or `~user`.
    """
    if val is None: # happens at startup
      val = ""
    val = os.path.expanduser(val)
    if val != self._dirpath:
      self._dirpath = val
      self._xds = None
      self._timefloats = None
      self.Modified()
  
  
  @smproperty.stringvector(name="FilenamePattern", default_values="prefix%02d.nc")
  def SetFilenamePattern(self, val):
    """ Pattern (Python format) that determines the files to be selected from the directory
    
    Notice that the filenames should not contain a "%".
    """
    if val != self._filenamePattern:
      self._filenamePattern = val
      self._xds = None
      self._timefloats = None
      self.Modified()
  
  
  @smproperty.intvector(name="FilenameRange", default_values=[0, 0, 1])
  def SetFilenameRange(self, begin, end, step):
    """ Range of integers used for the filename pattern (begin, end, step) """
    val = (begin, end, step)
    if val != self._filenameRange: # Python does in fact do an element-wise comparison
      self._filenameRange = val
      self._xds = None
      self._timefloats = None
      self.Modified()
  
  #------------------------------------------------------------------------------------------------#
  
  @smproperty.doublevector(name="TimestepValues", information_only="1", 
                           si_class="vtkSITimeStepsProperty")
  def GetTimestepValues(self):
    return self.get_timesteps()
  
  
  # This method must return a `vtkDataArraySelection` instance.
  @smproperty.dataarrayselection(name="Arrays")
  def GetDataArraySelection(self):
    return self.get_array_selection()
  
  #------------------------------------------------------------------------------------------------#
  
  def get_timesteps(self):
    self.get_xds()
    return self._timefloats
  
  
  def get_array_selection(self):
    return self._arrayselection
  
  #------------------------------------------------------------------------------------------------#
  
  def get_xds(self, requested_time=None):
    if self._xds is not None:
      if requested_time is not None:
        #obj = self._timefloat2object[requested_time]
        ##print("Selecting", self._timeDimName, requested_time, ":", obj)
        #return self._xds.sel({self._timeDimName: obj})
        return self._xds.sel({self._timeDimName: requested_time})
      return self._xds
    
    self._timefloats = None
    self._xvalues = numpy_support.numpy_to_vtk(np.array([0.]))
    self._yvalues = numpy_support.numpy_to_vtk(np.array([0.]))
    self._zvalues = numpy_support.numpy_to_vtk(np.array([0.]))
    
    if self._filenamePattern == "":
      raise RuntimeError("No filename pattern specified")
    
    dirpath = os.path.expanduser(self._dirpath)
    filenamePattern = self._filenamePattern
    filepaths = [os.path.join(dirpath, filenamePattern % i) for i in range(*self._filenameRange)]
    if not filepaths:
      return None
    # TODO use (time) chunks
    timeChunkSize = None
    chunks = None if timeChunkSize is None else dict(time=timeChunkSize)
    xds = None
    try:
      xds = xr.open_mfdataset(
        filepaths, concat_dim=self._memberDimName, combine="nested", chunks=chunks, decode_cf=False)
    except FileNotFoundError as e:
      print(e, sep="\n")
      return None
    
    # As of July 2020, xarray does not really support cell data, so we do neither.
    xds = xds.drop_dims(filter(lambda name: "bnds" in name, xds.dims.keys()))
    # Dropping a dimension automatically also drops any variable depending on this dimension.
    
    #print("All non-boundary-related dimensions with their number of samples:")
    nums = xds.sizes
    sep = ", "
    #print(sep.join(["%s=%d" % (name, n) for name, n in nums.items()]))
    
    numPts = nums[self._memberDimName] # to be extended below
    dimNames = set((self._memberDimName,)) # to be extended below
    if self._timeDimName in nums:
      numPts *= nums[self._timeDimName]
      dimNames.add(self._timeDimName)
      
    xDimName = "x" if "x" in nums else ("lon" if "lon" in nums else "")
    yDimName = "y" if "y" in nums else ("lat" if "lat" in nums else "")
    zDimName = "z" if "z" in nums else ("height" if "height" in nums else "")
    
    if xDimName != "":
      numPts *= nums[xDimName]
      dimNames.add(xDimName)
      self._xvalues = numpy_support.numpy_to_vtk(xds[xDimName].values)
    
    if yDimName != "":
      numPts *= nums[yDimName]
      dimNames.add(yDimName)
      self._yvalues = numpy_support.numpy_to_vtk(xds[yDimName].values)
    
    if zDimName != "":
      numPts *= nums[zDimName]
      dimNames.add(zDimName)
      self._zvalues = numpy_support.numpy_to_vtk(xds[zDimName].values)
    
    #print("Supported dimensions:", dimNames)
    namesToAdd = []
    for name, xda in xds.data_vars.items():
      #print("Attribute", name)
      myNumPts = reduce(lambda acc, val: acc * val, xda.sizes.values(), 1)
      if myNumPts != numPts:
        #print("--> disregarding it because its %d < %d" % (myNumPts, numPts))
        continue
      if not dimNames.issubset(xda.dims):
        #print("--> disregarding it because of its too few dimensions: ", list(xda.dims))
        # It is okay if there are more dimensions though (with only one sample).
        continue
      namesToAdd.append(name)
    
    arrSel = self._arrayselection
    indicesToRemove = [] # delete old attributes (by index) if necessary
    for i in range(arrSel.GetNumberOfArrays()):
      name = arrSel.GetArrayName(i)
      if name not in namesToAdd:
        indicesToRemove.append(i)
      else:
        namesToAdd.remove(name) # need not be added again
    for i in indicesToRemove:
      print("Removing old attribute *%s*" % name)
      arrSel.RemoveArrayByIndex(i)
    for name in namesToAdd:
      #print("Adding new attribute *%s*" % name)
      arrSel.AddArray(name)
    
    self._xds = xds
    if self._timeDimName in xds.dims:
      # To be safely in sync with other ParaView netCDF readers we have currently set
      # decode_cf=False in the call to xr.open_mfdataset
      #objects = xds[self._timeDimName].values
      #if "units" in xds[self._timeDimName].encoding:
        #unit, ref = xds[self._timeDimName].encoding["units"].split(" since ", 1)
        ##print("Dealing with %s since %s" % (unit, ref))
        #if ref[-1] == "Z":
          #ref = ref[0:-1]
        #needToAdd = pd.to_datetime("1970-01-01") - pd.to_datetime(ref)
        #objects += needToAdd
      #self._timefloats = objects.astype("float") / NS_PER_DAY # since 1970-01-01
      #self._timefloat2object = { flt: obj for flt, obj in zip(self._timefloats, objects) }
      self._timefloats = xds[self._timeDimName].values
    return self.get_xds(requested_time)
  
  #------------------------------------------------------------------------------------------------#
  
  def get_update_time(self, outInfo):
    executive = self.GetExecutive()
    timesteps = self.get_timesteps()
    if timesteps is None or len(timesteps) == 0:
      return None
    elif outInfo.Has(executive.UPDATE_TIME_STEP()) and len(timesteps) > 0:
      utime = outInfo.Get(executive.UPDATE_TIME_STEP())
      dtime = timesteps[0]
      for atime in timesteps:
        if atime > utime:
          return dtime
        else:
          dtime = atime
      return dtime
    else:
      assert(len(timesteps) > 0)
      return timesteps[0]
  
  #------------------------------------------------------------------------------------------------#
  
  def RequestInformation(self, request, iInfoVecTuple, oInfoVec):
    #print("--> BEGIN", type(self).__name__, "RequestInformation")
    executive = self.GetExecutive()
    oInfo = oInfoVec.GetInformationObject(0)
    oInfo.Remove(executive.TIME_STEPS())
    oInfo.Remove(executive.TIME_RANGE())
    
    timesteps = self.get_timesteps()
    if timesteps is not None:
      for time in timesteps:
        oInfo.Append(executive.TIME_STEPS(), time)
      oInfo.Append(executive.TIME_RANGE(), timesteps[0])
      oInfo.Append(executive.TIME_RANGE(), timesteps[-1])
    #print("<-- END", type(self).__name__, "RequestInformation")
    return 1
  
  #------------------------------------------------------------------------------------------------#
  
  def RequestData(self, request, iInfoVecTuple, oInfoVec):
    #print("--> BEGIN", type(self).__name__, "RequestData")
    oDataSet = dsa.WrapDataObject(vtkMultiBlockDataSet.GetData(oInfoVec, 0))
    dataTime = self.get_update_time(oInfoVec.GetInformationObject(0))
    if dataTime is not None:
      oDataSet.GetInformation().Set(oDataSet.DATA_TIME_STEP(), dataTime)
    xds = self.get_xds(dataTime)
    if xds is None:
      print("Multiblock Dataset from netCDF Files has failed. Have you set the parameters correctly?")
      # outputting a plain dataset rather than causing an error (potential PV crash)
      #oBlock = vtkRectilinearGrid()
      #oDataSet.SetBlock(0, oBlock)
      return 1
    
    # TODO set oDataSet field data from xds attributes
    
    numSamplesPerDim = (
      self._xvalues.GetNumberOfValues(),
      self._yvalues.GetNumberOfValues(),
      self._zvalues.GetNumberOfValues()
    )
    
    membDimName = self._memberDimName
    arrSel = self._arrayselection
    numArrs = arrSel.GetNumberOfArrays()
    for b in range(xds.sizes[membDimName]):
      xdsSub = xds.isel({membDimName: b})
      oBlock = vtkRectilinearGrid()
      oDataSet.SetBlock(b, oBlock)
      oBlock.SetXCoordinates(self._xvalues)
      oBlock.SetYCoordinates(self._yvalues)
      oBlock.SetZCoordinates(self._zvalues)
      oBlock.SetDimensions(numSamplesPerDim) # this *is* necessary
      #print("oBlock.GetNumberOfPoints():", oBlock.GetNumberOfPoints())
      pointData = oBlock.GetPointData()
      for i in range(numArrs):
        name = arrSel.GetArrayName(i)
        if arrSel.ArrayIsEnabled(name):
          vtkArr = numpy_support.numpy_to_vtk(xdsSub[name].values.ravel())
          vtkArr.SetName(name)
          pointData.AddArray(vtkArr)
      #print("Done adding block", b)
    
    #print("<-- END", type(self).__name__, "RequestData")
    return 1
